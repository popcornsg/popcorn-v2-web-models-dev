<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mastermovienaming_m extends MY_Model{
	
	protected $table = 'master_movie_naming';
	protected $primary_key = 'ID';
	protected $columns = array(
		'MasterMovieID' => array('MasterMovieID', 'trim|required'),
		'CountryCode' => array('CountryCode', 'trim|required'),
		'PossibleName' => array('PossibleName', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	
	
	public function set_filter($filter)
	{
		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('c.CountryCode', $country_code);
	}

	public function get_items($masterid, $filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("c.*")
				->from("$this->table c")
				->where("c.MasterMovieID", $masterid)
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function get_count($masterid, $filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$this->db->where("c.MasterMovieID", $masterid);
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	function insert_possible_naming($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}
}
