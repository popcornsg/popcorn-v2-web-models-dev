<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Push_notification_m extends MY_Model{

	protected $table = 'push_notifications';
	protected $primary_key = 'id';
	protected $columns = array(
		'country_code' => array('Country Code', 'trim|required'),
		'push_type' => array('Push Type', 'trim|required'),
        'title' => array('Title', 'trim|required'),
        'message' => array('Message', 'trim|required'),
        'status' => array('Status Push', 'trim', NULL, 1),
        'scheduleddate' => array('Scheduled Date', ''),
		'scheduledhour' => array('Scheduled Hour', ''),
		'scheduledminute' => array('Scheduled Minute', '')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($data){
		$result = $this->db->insert($this->table, $data);
		$lastid = $this->db->insert_id();
		return $lastid;
    }

	public function getById($pushid){
		$this->db->select("*")
				->from("$this->table")
				->where('id', $pushid);
	
		$query = $this->db->get();
		return $query;
	}

	//Backend functionality:
	public function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("p.*")
				->from("$this->table p")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by("created_date", "desc");
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function set_filter($filter)
	{ 
		$status = element('status', $filter, 1);
		$this->db->where('p.status', $status);

		$push_type = element('push_type', $filter, -1);
		if($push_type != -1){
			$this->db->where('p.push_type', $push_type);
        }
        
        $country_code = element('country_code', $filter, 'SG');
		$this->db->where('country_code', $country_code);
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table p");
		$row =  $query->row();
		return $row->num;
	}

	function get_record_complete($id){
		$this->db->select("p.*")
				->from("$this->table r")
				->where('p.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return null;
	    }
	}

	function update_by_id($id, $data){
		$this->db->update($this->table, $data, "id = ".$id);
		return $this->db->affected_rows();
	}

}