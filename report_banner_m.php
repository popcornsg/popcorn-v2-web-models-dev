<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_banner_m extends MY_Model{

	protected $table = 'report_banner';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function getAllValidBannerCampaign(){
		$this->dbTracking->select("code_tracking")
				->from("report_banner");

		$query = $this->dbTracking->get();
		return $query->result_array();
	}

	function getBannerReport($codetracking)
	{ 
		$sql = "SELECT ra.code_tracking, ra.unique_impression as TotalSample, ra.banner_impression, ra.banner_clicks, ra.booking_clicks,
		ra.unique_booking_clicks, ra.purchase_clicks, ra.unique_purchase_clicks from report_banner ra
		where ra.code_tracking = '".$codetracking."'";

		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

	function getBannerReports($codetrackings)
	{ 
		$cds = implode(",", $codetrackings);
		
		$sql = "SELECT ra.code_tracking, ra.unique_impression as TotalSample, ra.banner_impression, ra.banner_clicks, ra.booking_clicks,
		ra.unique_booking_clicks, ra.purchase_clicks, ra.unique_purchase_clicks from report_banner ra
		where ra.code_tracking in (".$cds.")";

		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}
}