<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class App_auth extends CI_Model
{	
	public function __construct() {
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
        $this->table = 'user_tokens';
        
		$this->_flushExpiredTokens(); // Remove expired tokens from table
	}
	
	public function generateToken($user_id, $expiry = 365, $guest = false) {
		$this->_removeExisting($user_id, $guest);
                $data = array();
                if ($guest) {
                    $guest_token = $this->_generateGuestToken($this->UDID);
                    $data['token'] = $guest_token['token'];
                    $data['user_id'] = $guest_token['user_id'];
                }
                else {
                    $data['token'] = $this->_generate($user_id);
                    $data['user_account_id']	= $user_id;
                }
		$data['expires']= date('Y-m-d H:i:s', (time() + (60*60*24*$expiry)));
		
		$this->_insertNewToken($data);
		
		return $data['token'];
	}
	
	public function validateToken($token, $expiry = 365) {
		$this->db->select('t.id, t.user_account_id, t.user_id, up.country, up.city, up.language, c.ShortCode');
		$this->db->where('expires >=', 'NOW()', false);
		$this->db->where('token', $token);
        $this->db->join('user_preferences up', 'up.user_account_id = t.user_account_id OR up.user_id = t.user_id', 'LEFT', false);
        $this->db->join('cities c', 'c.ID = up.city', 'LEFT', false);
		$query = $this->db->get($this->table . ' t');

		if (!$query->num_rows())
			return false;
		
		$user = $query->row_array();
                
		$return	= array();
		$return['user_id'] = $user['user_account_id'];
        $return['guest_id'] = $user['user_id'];
        $return['token'] = $token;
        $return['country'] = $user['country'];
        $return['city'] = $user['city'];
        $return['city_shortcode'] = $user['ShortCode'];
        $return['language'] = $user['language'];
		
        if ($user['user_account_id'] != null)
            $this->db->update($this->table, array('expires' => date('Y-m-d H:i:s', (time() + (60*60*24*$expiry)))), array('id' => $user['id']));
        
        return $return;
	}
	
	private function _generate($user_id) {
		$this->db->select('email');
		$query	= $this->db->get_where('user_accounts', array('id' => $user_id));
		$user	= $query->row_array();
		
		$key 	= hash_hmac('sha512', $user_id, $this->secret_key);
		$token	= substr(hash_hmac('sha512', $user['email'], $key), 0, 64);
		
		return $token;
	}
	
	private function _generateGuestToken($udid) {
		$this->db->select('id');
		$query	= $this->db->get_where('users', array('udid' => $udid));
		$user	= $query->row_array();
		
		$key 	= hash_hmac('sha512', $udid, $this->secret_key);
		$token	= substr(hash_hmac('sha512', $user['id'], $key), 0, 64);
                
                $guest_token['token'] = $token;
                $guest_token['user_id'] = $user['id'];
		
		return $guest_token;
	}
	
	private function _insertNewToken($data) {
		return $this->db->insert($this->table, $data);
	}
	
	private function _flushExpiredTokens() {
		$this->db->where('expires <', 'NOW()', false);
		return $this->db->delete($this->table);
	}

	private function _removeExisting($user_id, $guest) {
                if ($user_id == null)
                    return;
                
                if ($guest)
                    $where = array('user_id' => $user_id);
                else
                    $where = array('user_account_id' => $user_id);
                
		return $this->db->delete($this->table, $where);
	}
}
?>