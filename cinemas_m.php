<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cinemas_m extends MY_Model{
	
	protected $table = 'cinemas';
	protected $primary_key = 'ID';
	protected $columns = array(
		'GroupID' => array('GroupID', 'trim|required'),
		'CinemaName' => array('CinemaName', 'trim|required'),
		'CinemaSName' => array('CinemaSName', 'trim|required'),
		'CinemaIcon' => array('CinemaIcon', 'trim'),
		'Address' => array('Address', 'trim|required'),
		'GeoLat' => array('GeoLat', 'trim|required'),
		'GeoLong' => array('GeoLong', 'trim|required'),
		'ContactNumber' => array('ContactNumber', 'trim|required'),
		'IsActive' => array('IsActive', 'trim'),
	);	

	public function __construct()
	{
		parent::__construct();	
		$this->db = $this->load->database('default', TRUE);	
	}		

	public function set_filter($filter)
	{
		$status = element('status', $filter, 2);
		if($status !=2 ){
			$this->db->where('c.IsActive', $status);
		}
		if($name = element('name', $filter)){
			$this->db->like('c.CinemaName', $name);
			$this->db->or_like('c.CinemaSName', $name);
		}

	}

	public function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("c.*,g.GroupName")
				->from("$this->table c")
				->join("cinema_groups g", "g.ID=c.GroupID","left")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter,'ID')){
			$this->db->order_by($sort_col, element('sort_dir', $filter, 'DESC'));
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	public function cinema_groups($empty_value='')
	{
		$query = $this->db->get('cinema_groups');
		return $this->result_assoc_array($query, 'ID', 'GroupName', $empty_value);
	}

	public function get_cinema_groups($groupid)
	{
		$this->db->select("*")
				->from("cinema_groups")
				->where("ID", $groupid);	
		
		$query = $this->db->get();
		return $query;
	}

	public function get_by_term($term, $countrycode, $limit){
		$this->db->select("c.*")
				->from("$this->table c")
				->join("cities ct", "ct.ID = c.CityID")
				->where('ct.CountryCode', $countrycode)
				->like('c.CinemaName', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	public function get_items_by_group($groupid)
	{
		$this->db->select("*")
				->from("$this->table c")
				->where("GroupID", $groupid);		
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_groups($country_code)
	{
		$ccode = explode("-", $country_code);

		$this->db->select("*")
				->from("cinema_groups")
				->where("CountryCode", $ccode[0]);	
		
		$query = $this->db->get();
		return $query->result();
	}

	public function getGroupsByIds($groupids){
		$this->db->select("*")
				->from("cinema_groups")
				->where_in('ID', $groupids);

		$query = $this->db->get();
		return $query->result();
	}

	public function getCinemasByIds($cinemaids){
		$this->db->select("*")
				->from("cinemas")
				->where_in('ID', $cinemaids);

		$query = $this->db->get();
		return $query->result();
	}

	public function getCinemaWithinRadius($lat, $lng, $countrycode, $distance){
		$this->db->select('c.id, c.CinemaName, c.GeoLat, c.GeoLong')
				->from('cinemas c')
				->join('cinema_groups cg', 'c.GroupID = cg.ID')
				->where('cg.countrycode', $countrycode)
				->where('c.IsActive', 1)
				->where("(6371 * acos( cos( radians(c.GeoLat) ) * cos( radians( ".$lat." ) ) * cos( radians( ".$lng." ) - radians(c.GeoLong)) + sin( radians(c.GeoLat) ) * sin( radians( ".$lat." ) ) ) ) < ".$distance, NULL, false);
		
		$query = $this->db->get();
		return $query->result();
	}

	//mobile web pages:
	public function getCinemas($dynData, $countryCode = "") {
		$selectedCountryCode = "";
		if($countryCode != ""){
			$selectedCountryCode = $countryCode;
		}else{
			$selectedCountryCode = $this->COUNTRY;
		}

		$this->db->select('c.ID as CinemaID, cg.ID as GroupID, CinemaName, CinemaSName, Address, ContactNumber, GeoLat, GeoLong, GroupName');
		$this->db->from('cinemas c');
		$this->db->join('cinema_groups cg', 'c.GroupID = cg.ID');

		//check cityID:
		if($this->CITYID != 0){
			$this->db->where('c.CityID', $this->CITYID);
		}

		$this->db->where('cg.CountryCode', $selectedCountryCode);	
		$this->db->where(array('IsActive' => '1'));
        $this->db->order_by('CinemaID != 9', 'ASC', false);
        $this->db->order_by('CinemaID != 19', 'ASC', false);
		$this->db->order_by('CinemaName');
		$query	= $this->db->get();
		
		if (is_array($dynData)) {
			$cinemas = array();
			
			foreach ($query->result_array() as $c) {
				if (isset($dynData[((int)$c['CinemaID'])])) {
					$c['MovieIDs']	= $dynData[((int)$c['CinemaID'])];
				} else {
					$c['MovieIDs'] 	= array();
				}	

				$cinemas[] = $c;
			}

			return $cinemas;
		}
		
		return $query->result_array();
	}

	public function getFavCinemas($udid) {
		$this->db->select('cf.UDID, cf.CinemaID, cf.Platform, cf.CreatedDate, cf.LastUpdate');
		$this->db->from('cinema_favourites cf');
		$this->db->join('cinemas c', 'cf.CinemaID = c.ID');
		$this->db->join('cinema_groups cg', 'c.GroupID = cg.ID');
		$this->db->where('cg.CountryCode', $this->COUNTRY);	
		$query = $this->db->get();

		return $query->result_array();
	}
}
