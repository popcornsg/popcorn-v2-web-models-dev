<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_user_m extends MY_Model{
	
	protected $table = 'web_users';

	public function __construct()
	{
		parent::__construct();	
		$this->db = $this->load->database('default', TRUE);	
	}	

	public function getTotalTrafficToApp($from, $to, $source){
		$dateFrom = date($from." h:i:s");
		$dateTo = date($to." h:i:s");

		$sql = "select count(distinct wu.app_user_id) as Total, wu.app_platform as Platform from web_raw_logs wr
join web_users wu on wr.udid = wu.udid 
where wr.url like '%rogueone' and app_user_id is not null and wr.referer like '%".$source."%' and wr.created_date > '".$dateFrom."' and wr.created_date < '".$dateTo."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}	

	public function getTotalTrafficFrom($from, $to, $source){
		$dateFrom = date($from." h:i:s");
		$dateTo = date($to." h:i:s");

		$sql = "select count(wr.udid) as Total from web_raw_logs wr
join web_users wu on wr.udid = wu.udid 
where wr.url like '%rogueone' and wr.referer like '%".$source."%' and wr.created_date > '".$dateFrom."' and wr.created_date < '".$dateTo."'";
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function getTotalWatchTrailer($from, $to, $movieid, $source){
		$dateFrom = date($from." h:i:s");
		$dateTo = date($to." h:i:s");

		$sql = "Select count(distinct wu.web_user_id) as Total from web_raw_logs wr
join web_user_trailers wu on wr.udid = wu.web_user_id 
where wr.url like '%rogueone' and wu.movie_id = ".$movieid." and wr.referer like '%".$source."%' and wr.created_date > '".$dateFrom."' and wr.created_date < '".$dateTo."'";
		
		$query = $this->db->query($sql);
		return $query->row();
	}	

	public function update_push($udid){
		$this->db->update($this->table, array("push_enabled" => 1), array("udid" => $udid));
	}
}
