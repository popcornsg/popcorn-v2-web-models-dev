<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking_m extends MY_Model
{
	public function __construct()
	{
		parent::__construct();	
	}

	public function insert_tracking_promo($promocode, $udid, $platform, $version, $countrycode, $userid = null){
		$this->db2 = $this->load->database('tracking', TRUE);

		$data = array();
		$data["PromoCode"] = $promocode;
		$data["Platform"] = $platform;
		$data["Version"] = $version;
		$data["UDID"] = $udid;
		$data["CountryCode"] = $countrycode;
		if(isset($userid)) $data["UserID"] = $userid;

		return $this->db2->insert("tracking_promos", $data);
	}

}
?>