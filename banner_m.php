<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_m extends MY_Model{

	protected $table = 'banners';
	protected $primary_key = 'id';
	protected $columns = array(
		'target' => array('Target Page', 'trim|required'),
		'dataid' => array('Data', 'trim', NULL, 0),
		'country_code' => array('Country', 'trim|required'),
		'banner_type' => array('Banner Type', 'trim', NULL, 3),
		'order_banner' => array('Order Banner', 'trim', NULL, 1),
		'banner_title' => array('Banner Title', 'trim', NULL, ""),
		'banner_url' => array('Banner Url', 'trim', NULL, ""),
		'banner_position' => array('Banner Position', 'trim', NULL, 1),
		'banner_content' => array('Banner Content', 'trim', NULL, "", FALSE),
		'banner_image' => array('Banner Image', 'trim'),
		'banner_thumb' => array('Banner Thumb', 'trim')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function getBannerPosition(){
		$type = array();
		$type[0]["value"] = 1;
		$type[0]["text"] = "Main Banner";
		$type[1]["value"] = 2;
		$type[1]["text"] = "Featured Banner";
		$type[2]["value"] = 3;
		$type[2]["text"] = "Middle Ads Banner";
		$type[3]["value"] = 4;
		$type[3]["text"] = "Footer Ads Banner";

		return $type;
	}

	function getBannerType(){
		$type = array();
		$type[0]["value"] = 3;
		$type[0]["text"] = "URL";
		$type[1]["value"] = 1;
		$type[1]["text"] = "Movie";
		$type[2]["value"] = 2;
		$type[2]["text"] = "Article";

		return $type;
	}

	function set_filter($filter)
	{ 
		$target = element('target', $filter, 1);
		$this->db->where('b.target', $target);
		$status = element('status', $filter, 2);
		if($status != 2 ){
			$this->db->where('b.status', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('b.country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("b.*")
				->from("$this->table b")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('created_date', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("b.*")
				->from("$this->table b")
				->where('b.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_items_by_target($target, $ctr){
		$country_raw = explode("-", $ctr);
		$country = $country_raw[0];

		$this->db->select("b.*")
				->from("$this->table b")
				->where('b.status', 1)
				->where('b.country_code', $country)
				->where('b.target', $target);
                if (isset($this->client_version) && $this->client_version == "5.07")
                    $this->db->where_not_in('b.dataid', array(339,337,330,325,324,322,321,320,317,312,311,309,308,305,297,296,291,285,276,258));

		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table b");
		$row =  $query->row();
		return $row->num;
	}

	function get_recent_gallery(){
		$this->db->select("banner_image")
				->from("banners")
				->where('banner_image !=', "")
				->where('status', 1)
				->order_by('created_date', 'DESC')
				->limit(15, 0);

		$query = $this->db->get();
		return $query->result();
	}
}