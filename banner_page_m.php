<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_page_m extends MY_Model{

	protected $table = 'app_banner_page';
	protected $primary_key = 'id';
	protected $columns = array(
		'CountryCode' => array('Country', 'trim|required'),
		'FeaturedImgURL' => array('Banner Image', 'trim|required'),
		'ImgThumb' => array('Banner Thumb Im', 'trim'),
		'Location' => array('Location', 'trim|required'),
		'Selector' => array('Selector', 'trim|required'),
		'Type' => array('Type', 'trim|required'),
		'MediaType' => array('Media Type', 'trim', NULL, NULL),
		'DataID' => array('Data ID', 'trim', NULL, 0),
		'DataURL' => array('Data URL', 'trim', NULL, ""),
		'DataCaramelURL' => array('Data Caramel URL', 'trim', NULL, ""),
		'CodeTracking' => array('Code Tracking', 'trim', NULL, "0"),
		'Order' => array('Order Banner', 'trim', NULL, 1),
		'IsAB' => array('Is A/B Testing', 'trim', NULL, 0),
		'StartDate' => array('Is A/B Testing', 'trim|required'),
		'EndDate' => array('Is A/B Testing', 'trim|required')		
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function getBannerMediaType(){
		$type = array();
		$type[0]["value"] = "image";
		$type[0]["text"] = "Image";
		$type[1]["value"] = "video";
		$type[1]["text"] = "Video";
		$type[2]["value"] = "website";
		$type[2]["text"] = "Website";
		return $type;
	}

	function getBannerLocation(){
		$type = array();
		$type[0]["value"] = "nowshowing";
		$type[0]["text"] = "Now Showing";
		$type[1]["value"] = "upcoming";
		$type[1]["text"] = "Up Coming";
		return $type;
	}

	function getBannerSelector(){
		$type = array();
		$type[0]["value"] = "movie";
		$type[0]["text"] = "Movie";
		$type[1]["value"] = "cinema";
		$type[1]["text"] = "Cinema";
		$type[2]["value"] = "URL";
		$type[2]["text"] = "URL";
		return $type;
	}

	function getBannerType(){
		$type = array();
		$type[0]["value"] = "detail";
		$type[0]["text"] = "Detail";
		$type[1]["value"] = "showtime";
		$type[1]["text"] = "Showtime";
		$type[2]["value"] = "URL";
		$type[2]["text"] = "URL";
		return $type;
	}

	function set_filter($filter)
	{ 
		$location = element('location', $filter, 1);
		$this->db->where('b.Location', $location);
		$status = element('status', $filter, 3);
		if($status != 3 ){
			$this->db->where('b.IsActive', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('b.CountryCode', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("b.*")
				->from("app_banner_page b")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('last_updated', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("b.*")
				->from("app_banner_page b")
				->where('b.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_items_by_location($location, $country){
		$this->db->select("b.*")
				->from("app_banner_page b")
				->where('b.IsActive', 1)
				->where('b.CountryCode', $country)
				->where('b.Location', $location);

		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("app_banner_page b");
		$row = $query->row();
		return $row->num;
	}

	function get_recent_gallery(){
		$this->db->select("FeaturedImgURL")
				->from("app_banner_page")
				->where('MediaType', 'image')
				->order_by('last_updated', 'DESC')
				->limit(15, 0);

		$query = $this->db->get();
		return $query->result();
	}
}