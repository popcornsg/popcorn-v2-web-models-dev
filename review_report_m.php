<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review_report_m extends MY_Model{

	protected $table = 'review_reports';
	protected $primary_key = 'id';
	protected $columns = array(
		'userid' => array('Movie', 'trim|required'),
		'reviewid' => array('Rating', 'trim|required'),
		'report_title' => array('Report', 'trim|required'),
		'report_detail' => array('Report Detail', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($data){
		return $this->db->insert('review_reports', $data);
	}

	public function getById($reportid){
		$this->db->select("*")
				->from("$this->table")
				->where('id', $reportid);

		$query = $this->db->get();
		return $query;
	}

	//Backend functionality:

	public function get_items($reviewid, $filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("r.*, u.name AS username")
				->from("$this->table r")
				->where('r.reviewid', $reviewid)
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function set_filter($filter)
	{ 
		$this->db->join('email_user u', 'r.userid = u.id');
	}

	function get_count($reviewid, $filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$this->db->where('reviewid', $reviewid);
		$query = $this->db->get("$this->table r");
		$row =  $query->row();
		return $row->num;
	}

}