<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Model
{
	function merchant_options($empty_value='')
	{
		$query = $this->db->get('merchant');
		return $this->result_assoc_array($query, 'id', 'name', $empty_value);
	}

	function category_options($empty_value='')
	{
		$query = $this->db->get('category_dict');
		return $this->result_assoc_array($query, 'id', 'name', $empty_value);
	}

	function category_tags()
	{
		$query = $this->db->get('category_dict');
		return $this->result_assoc_array($query, 'name');
	}

	function type_tags()
	{
		$query = $this->db->get('type_dict');
		return $this->result_assoc_array($query, 'name');
	}

	function platform_options($empty_value='')
	{
		$query = $this->db->get('platform_dict');
		return $this->result_assoc_array($query, 'id', 'name', $empty_value);
	}

	
	public function get_merchant_locations($merchant_id)
	{
		if($merchant_id){
			$query = $this->db->get_where('locations', array('merchant_id'=>$merchant_id));
			return $query->result();
		}
		return array();
	}

}
