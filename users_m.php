
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends MY_Model{
	
	protected $table = 'users';
	protected $primary_key = 'id';
	protected $columns = array(
		'active'	=> array('Active', 'trim'),
		'first_name' => array('First Name', 'trim|required'),
		'last_name' => array('Last Name', 'trim|required'),
		'email' => array('Email', 'trim|required'),	
		'username' => array('Username', 'trim|required|alpha_dash'),
	);	

	/**
	 * Validate fields and return fields required to save in database
	 */
	function get_from_post($cols=NULL, $id)
	{
		$password = $this->input->post('password');
		
		//new user or password changed
		if(!$id || $password){ 
			$this->columns['password'] = array('Password', 'trim|required|min_length[6]');
			$this->columns['confirm_password'] = array('Confirm Password', 'trim|required|matches[password]', FALSE);
			$this->columns['salt'] = array('Salt', 'trim');
		}

		//for new user
		if(!$id){ 
			$this->columns['email'][1] .="|is_unique[users.email]"; 	// add unique email validation rule
			$this->columns['username'][1] .="|is_unique[users.username]"; 	// add unique email validation rule
			$this->columns['activation_key'] = array('ActivationKey', 'trim');
		}		

		//validate fields
		$fields = parent::get_from_post($cols);

		//generate password hash and salt
		if($fields && $password){
			$fields['salt'] = $salt = substr(md5(rand()), 0, 8);
			$fields['password'] = password_hash($password,$salt);
		}
		return $fields;
	}
	
	function set_filter($filter)
	{
		$status = element('status', $filter, 2);
		if($status !=2 ){
			$this->db->where('c.active', $status);
		}
		if($name = element('name', $filter)){
			$this->db->like('c.first_name', $name);
		}

	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("c.*")
				->from("$this->table c")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	function get_user($id)
	{
		if(!$id){
			return $this->get_empty_record();
		}

		$query = $this->db->select("u.*, CONCAT(u.first_name,' ', u.last_name) AS full_name", FALSE)
						->from("$this->table u")						
						->where('u.id', $id)
						->get();
		if($query->num_rows()){
			return $query->row();
		}
		return FALSE;
	}

	function autocomplete_users($q){
		$this->db->select("id, CONCAT(first_name, ' ', last_name) AS text", FALSE)
			->from($this->table)
			->like("CONCAT(username, ' - ', first_name, ' ', last_name)",  $q)
			->limit(25);

		$query  = $this->db->get();
		if($query->num_rows){
			return $query->result();
		}
		else{
			return array(''=>'No match found');
		}
	}

	function users_select2($coma_values)
	{
		if(!$coma_values){
			return array();
		}
		$this->db->select("id, CONCAT(first_name, ' ', last_name) AS text", FALSE)
					->from($this->table)
					->where("id IN ($coma_values)");
		$query = $this->db->get();
		return $query->result();

	}

	function request_new_email($user_id, $new_email)
	{
		//generate activation key
		$this->load->helper('string');		
		$email_key = random_string('alnum', 10); 

		$fields = array(
			'new_email' => $new_email,
			'new_email_key' => $email_key,
		);

		return $this->update_record($fields, $user_id);
	}

	public function get_movie_fav($movieid)
	{	
		$this->db->select("um.id, um.user_id, u.udid")
				->from("user_movie_favs um")
				->where('um.movie_id', $movieid)
				->where('um.notified', 0)
				->where('um.last_updated >', '2016-06-1 12:00:00') //since last month 2016
				->join("users u", "um.user_id = u.id");
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_showtimes_fav($showtimeid)
	{	
		$this->db->select("us.id, us.user_id, u.udid")
				->from("user_showtime_favs us")
				->where('us.showtime_id', $showtimeid)
				->where('us.notified', 0)
				->where('us.last_updated >', '2016-06-1 12:00:00') //since last month 2016
				->join("users u", "us.user_id = u.id");
		
		$query = $this->db->get();
		return $query->result();
	}

	public function update_movie_fav($ids){
		$now = date("Y-m-d H:i:s");
		$data = array("notified" => 1, "notified_date" => $now);
		$this->db->where_in('id', $ids);
		$this->db->update('user_movie_favs', $data);
		return $this->db->affected_rows();
	}
	
	public function update_showtimes_fav($ids){
		$now = date("Y-m-d H:i:s");
		$data = array("notified" => 1, "notified_date" => $now);
		$this->db->where_in('id', $ids);
		$this->db->update('facebook_showtime_favs', $data);
		return $this->db->affected_rows();
	}
        
    public function set_preference($pref) {
        // Check if a record exists
        if ($this->user_id)
            $this->db->or_where('user_account_id', $this->user_id);
        else if ($this->guest_id)
            $this->db->or_where('user_id', $this->guest_id);
        else
            return false;
        $check_query = $this->db->get_where('user_preferences');
        
        // If record exists, update record
        if ($check_query->num_rows() == 1) {
            $row = $check_query->row();
            
            $this->db->update('user_preferences', $pref, array('id' => $row->id));
        }
        // Else insert new record
        else {
            if ($this->user_id)
                $pref['user_account_id'] = $this->user_id;
            if ($this->guest_id)
                $pref['user_id'] = $this->guest_id;
            
            $this->db->insert('user_preferences', $pref);
        }
        
        return array("status" => "OK", "title" => "", "message" => "", "token" => $this->access_token);
    }
}
