<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_top_movies_m extends MY_Model{
	
	protected $table = 'user_top_movies';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function get_my_top_movies($udid, $userid = 0){
		if($userid > 0){
			$this->db->select("m.*, utm.udid, utm.totalclick, CASE w.status WHEN 1 THEN 1 ELSE 0 END as 'isNotWatchList'", false)
				->from("$this->table utm")
				->join("movies m", "m.id = utm.movieid")
				->join("watchlist w", "w.movie_id = m.id AND w.user_id = ".$userid, 'LEFT');
		}else{
			$this->db->select("m.*, utm.udid, utm.totalclick, 0 as 'isNotWatchList'", false)
			->from("$this->table utm")
			->join("movies m", "m.id = utm.movieid");
		}

		$this->db->where('utm.udid', $udid)
				->where('utm.responded', 0)
				->order_by('utm.totalclick', 'DESC')
				->limit(5, 0);

		$query = $this->db->get();
		return $query->result();
	}

	function do_response($udid, $movieid, $skip = 0){
		$response = 1;
		if($skip == 1){
			$response = 2;
		}
		$this->db->update($this->table, array("responded" => $response), array("udid" => $udid, "movieid" => $movieid));
		return $this->db->affected_rows();
	}

}
