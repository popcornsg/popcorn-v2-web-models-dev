<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_view_m extends MY_Model{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	
	
	public function checkWasVisitor($movieid, $userip){
		$this->db->select("*")
				->from("movie_views")
				->where('movie_id', $movieid)
				->where('userip', $userip);

		$query = $this->db->get();
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function insert_movie_view($data){
		if($this->db->insert("movie_views", $data))
		{
			$this->db->select("*")
				->from("movie_total_views")
				->where('movie_id', $data['movie_id']);

			$query = $this->db->get();
			if($query->num_rows() > 0){
				//update:
				$this->db->where('movie_id', $data['movie_id']);
				$this->db->set('counter', 'counter + 1', FALSE);
				$this->db->update('movie_total_views');
			}else{
				$total["movie_id"] = $data['movie_id'];
				$total["counter"] = 1;
				$this->db->insert("movie_total_views", $total);
			}

			return true;
		}else{
			return false;
		}
	}
}
