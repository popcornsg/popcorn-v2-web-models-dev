<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_referred_m extends MY_Model{
	
	protected $table = 'user_referred';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($userid, $refcode) {
        $this->db->select("*")
				->from($this->table)
				->where('user_id', $userid)
				->where('joined_code', $refcode);
		$query = $this->db->get();
		
		if($query->num_rows() < 1){
			//safe to insert:
			$data["user_id"] = $userid;
			$data["joined_code"] = $refcode;
			$this->db->insert($this->table, $data);
			return true;
		}else{
			return false;
		}
    }

    public function getByUserId($userid){
		$this->db->select("*")
				->from($this->table)
				->where('user_id', $userid);

		$query = $this->db->get();
		return $query;
	}
}
