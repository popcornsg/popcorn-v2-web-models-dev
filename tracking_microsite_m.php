<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking_microsite_m extends MY_Model{

	protected $table = 'tracking_microsites';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function add_tracking($data)
	{ 
		return $this->dbTracking->insert($this->table, $data);
	}


}