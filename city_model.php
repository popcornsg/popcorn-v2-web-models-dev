<?php

class City_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();	
		$this->db = $this->load->database('default', TRUE);	
	}

	public function getCountries() {
		$this->db->select('ct.CountryCode, cr.CountryName');
		$this->db->from('cities ct');
		$this->db->join('countries cr', 'cr.CountryCode = ct.CountryCode');
		$this->db->group_by(array('ct.CountryCode', 'cr.CountryName'));
		$query= $this->db->get();
		
		return $query->result_array();
	}
	
	public function getCities() {
		$this->db->select('ID, CityName, ShortCode, CountryCode');
		$this->db->where('Status', 1);
		$this->db->from('cities');
		$this->db->order_by('CityName');
		$query= $this->db->get();
		
		return $query->result_array();
	}
}
?>