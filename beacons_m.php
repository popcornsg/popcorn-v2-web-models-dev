<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beacons_m extends MY_Model{
	
	protected $table = 'beacons_analytics';

	public function __construct()
	{
		parent::__construct();	
		$this->db = $this->load->database('default', TRUE);	
	}		

	public function getBeaconDailyVisit($uuid){
		$now = date("Y-m-d h:i:s");
		$date = date("Y-m-d 00:00:00", strtotime("-7 days", strtotime($now)));

		/*$this->db->_protect_identifiers = FALSE;
		$this->db->select('DATE_FORMAT(triggered_date, "%Y-%m-%d") as DT, count(udid) as Total')
			->where('triggered_date >', $date)
			->where('uuid', $uuid)
			->group_by(array('DT'))
			->order_by('triggered_date', 'asc');*/
		$sql = 'select a.DT, count(a.UDID) as Total
from (select distinct UDID, DATE_FORMAT(triggered_date, "%Y-%m-%d") as DT from beacons_analytics
where uuid = "'.$uuid.'"
and triggered_date > "'.$date.'") as a
group by DT order by DT asc';
		$query = $this->db->query($sql);

		//$query = $this->db->get('beacons_analytics');
		return $query->result_array();
	}	

}
