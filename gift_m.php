<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gift_m extends MY_Model{

	protected $table = 'gifts';
	protected $primary_key = 'id';
	protected $columns = array(
		'country_code' => array('Country Code', 'trim|required'),
		'gift_name' => array('Gift name', 'trim|required'),
		'gift_image' => array('Gift images', 'trim|required'),
		'claimed_points' => array('Claimed points', 'trim|required'),
		'actual_price' => array('Actual price', 'trim', NULL, 0),
		'stock' => array('Stock', 'trim', NULL, 0),
		'gift_description' => array('Description', 'trim', NULL, "")
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function set_filter($filter)
	{ 
		$status = element('status', $filter, 2);
		if($status != 2){
			$this->db->where('g.status', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('g.country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("g.*")
				->from("$this->table g")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('created_date', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("g.*")
				->from("$this->table g")
				->where('g.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function getAllRedemption($countrycode){
		$this->db->select("g.*")
				->from("$this->table g")
				->where('status', 1)
				->where('country_code', $countrycode)
				->order_by('claimed_points', 'DESC');

		$query = $this->db->get();
		return $query->result(); 
	}

	function getById($giftid){
		$this->db->select("*")
				->from("$this->table")
				->where('status', 1)
				->where('id', $giftid);

		$query = $this->db->get();
		return $query; 
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table g");
		$row =  $query->row();
		return $row->num;
	}

	function update_data($id, $data){
		$this->db->update($this->table, $data, "id = ".$id);
		return $this->db->affected_rows();
	}
}