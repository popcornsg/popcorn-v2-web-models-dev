<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gift_redemption_m extends MY_Model{

	/*
		Status Redemption:
		0 = Pending
		1 = Settled	
	*/

	protected $table = 'gifts_redemption';
	protected $primary_key = 'id';
	protected $columns = array(
		'country_code' => array('Country Code', 'trim|required'),
		'gift_id' => array('Gift Id', 'trim|required'),
		'user_id' => array('User Id', 'trim|required'),
		'deducted_points' => array('Deducted points', 'trim', NULL, 0)
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function set_filter($filter)
	{ 
		$status = element('status', $filter, 3);
		if($status != 3){
			$this->db->where('g.status', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('g.country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("g.*, ua.name, ua.identity_number, ua.mobile, ua.email, gf.gift_name, gf.gift_image")
				->from("$this->table g")
				->join("user_accounts ua", "g.user_id = ua.id")
				->join("gifts gf", "gf.id = g.gift_id")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('created_date', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("g.*")
				->from("$this->table g")
				->where('g.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function getAllRedemption($countrycode){
		$this->db->select("g.*")
				->from("$this->table g")
				->where('status', 1)
				->where('country_code', $countrycode);

		$query = $this->db->get();
		return $query->result(); 
	}

	function getById($giftid, $status = 1){
		$this->db->select("*")
				->from("$this->table")
				->where('id', $giftid);
		
		if($status == 1){
			$this->db->where('status', $status);
		}

		$query = $this->db->get();
		return $query; 
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table g");
		$row =  $query->row();
		return $row->num;
	}

	public function isValidToRedeem($userid, $giftid){
		$this->db->select("*")
				->from("$this->table")
				->where('status', 0)
				->where('gift_id', $giftid)
				->where('user_id', $userid);

		$query = $this->db->get();
		if($query->num_rows() == 0){
			return true;
		}else{
			return false;
		}
	}

	public function insert_data($data){
		if($this->db->insert($this->table, $data)){
			$id = $this->db->insert_id();
			if(!$id) $id = false;
			return $id;
		}else{
			return false;
		}
	}

	function add_back_point($userid, $deductedpoint){
		$this->db->where('id', $userid);
		$this->db->set('points', 'points + '.$deductedpoint, FALSE);
		$this->db->update('user_accounts');
	}

	function update_data($id, $data){
		$this->db->update($this->table, $data, "id = ".$id);
		return $this->db->affected_rows();
	}

	function getRedemptionHistory($userid, $offset, $limit){
		$this->db->select("g.*, ua.name, gf.gift_name, gf.gift_image")
				->from("$this->table g")
				->join("user_accounts ua", "g.user_id = ua.id")
				->join("gifts gf", "gf.id = g.gift_id")
				->order_by('created_date', 'DESC')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result_array();
	}
}