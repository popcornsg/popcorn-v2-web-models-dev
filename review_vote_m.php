<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review_vote_m extends MY_Model{

	protected $table = 'review_votes';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($data){
		return $this->db->insert('review_votes', $data);
	}

	public function getByUserIdAndReviewId($reviewid, $userid){
		$this->db->select("*")
				->from("$this->table")
				->where('review_id', $reviewid)
				->where('user_id', $userid);

		$query = $this->db->get();
		return $query;
	}
}