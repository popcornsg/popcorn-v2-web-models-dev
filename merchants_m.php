<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchants_m extends MY_Model{
	
	protected $table = 'merchant';
	protected $primary_key = 'id';
	protected $columns = array(
		'name' => array('Name', 'trim|required'),
		'image' => array('Image', 'trim|required'),
		'url' => array('Url', 'trim|required|prep_url'),
		'intro' => array('Intro', 'trim'),
		'description' => array('Description', 'trim|required'),
		'address' => array('Address', 'trim|required'),
		'latitude' => array('Latitude', 'trim'),
		'longitude' => array('Longitude', 'trim'),
		
		'contact_person' => array('Contact Person', 'trim|required'),
		'contact_number' => array('Contact Number', 'trim|required'),
		'contact_email' => array('Contact Email', 'trim|required|valid_email'),
		
		'last_updated' => array('Updated', 'trim|required'),


		);	
	

	
	function set_filter($filter)
	{
		$status = element('status', $filter, 2);
		if($status !=2 ){
			$this->db->where('c.active', $status);
		}
		if($name = element('name', $filter)){
			$this->db->like('c.name', $name);
		}

	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("c.*")
				->from("$this->table c")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter, 'id')){
			$this->db->order_by($sort_col, element('sort_dir', $filter, 'desc'));
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}



	public function save_locations($merchant_id, $locations)
	{	
		$addresses = element('address', $locations);
		if(!$addresses){ return FALSE;}

		$records = array();
		foreach($addresses as $index => $address)
		{			
			$rec['merchant_id'] = $merchant_id;
			$rec['address'] = $address;
			$rec['extra'] = $locations['extra'][$index];			
			
			//check if required update latitude,longitude
			$old_address = $locations['old_address'][$index];
			if($address != $old_address)
			{
				$coord = get_coordinates($address);
				$rec['latitude'] = $coord['lat'];
				$rec['longitude'] = $coord['lng'];
			}

			$id = $locations['id'][$index];
			if($id){ //update record
				$this->db->where('id', $id);
				$this->db->set($rec);
				$this->db->update('locations');			
			}	
			else{
				$this->db->insert('locations', $rec);
			}
		}
	}

	function remove_location($loc_id)
	{
		return $this->db->where('id', $loc_id)
					->limit(1)
					->delete('locations');
	}

}
