<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review_m extends MY_Model{

	protected $table = 'reviews';
	protected $primary_key = 'id';
	protected $columns = array(
		'movie_id' => array('Movie', 'trim|required'),
		'rating' => array('Rating', 'trim|required'),
		'review_detail' => array('Review', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($data){
		$result = $this->db->insert('reviews', $data);
		$lastid = $this->db->insert_id();
		return $lastid;
	}

	public function getByUDIDAndMovieId($udid, $movieid){
		$this->db->select("*")
				->from("$this->table")
				->where('udid', $udid)
				->where('movie_id', $movieid)
				->where('is_approved', 1);

		$query = $this->db->get();
		return $query;
	}

	public function getByUserAndMovieId($userid, $movieid){
		$this->db->select("r.*, ua.avatar_url as Avatar, ua.name as Name")
				->from("$this->table r")
				->join('user_accounts ua', 'r.user_id = ua.id')
				->where('r.user_id', $userid)
				->where('r.movie_id', $movieid)
				->where('r.is_approved', 1);

		$query = $this->db->get();
		return $query;
	}

	public function getByReviewByUser($reviewid){
		$this->db->select("r.*, ua.avatar_url as Avatar, ua.name as Name")
				->from("$this->table r")
				->join('user_accounts ua', 'r.user_id = ua.id')
				->where('r.id', $reviewid)
				->where('r.is_approved', 1);

		$query = $this->db->get();
		return $query;
	}

	function getReviewsByMovieId($movieid, $offset, $limit, $notin = false){
		$this->db->select("r.id, r.created_date, ua.avatar_url as Avatar, ua.name as Name, ua.id as UserID, r.review_title, r.review_detail, r.rating, SUM(rv.vote = 1) as Vote, SUM(rv.vote = -1) as VoteDown", false)
				->from("$this->table r")
				->where('r.movie_id', $movieid)
				->where('r.review_detail !=', '')
				->where('r.is_approved', 1);

		if(is_array($notin)){
			$this->db->where_not_in('r.id', $notin);
		}

		$this->db->order_by('Vote', 'DESC')
				->order_by('r.created_date', 'DESC')
				->join('user_accounts ua', 'r.user_id = ua.id')
				->join('review_votes rv', 'r.id = rv.review_id', 'left')
				->group_by('r.id, r.created_date, ua.avatar_url, ua.name, ua.id, r.review_title, r.review_detail, r.rating')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result();
	}

	function getReviewsByUserId($userid, $offset, $limit){
		$this->db->select("m.ID as movieid, m.MovieName, , m.MovieNameDisplay, m.ListingImage, r.id, r.created_date, ua.avatar_url as Avatar, ua.name as Name, ua.id as UserID, r.review_title, r.review_detail, r.rating, SUM(rv.vote = 1) as Vote, SUM(rv.vote = -1) as VoteDown", false)
				->from("$this->table r")
				->where('r.user_id', $userid)
				->where('r.is_approved', 1);

		$this->db->order_by('Vote', 'DESC')
				->order_by('r.created_date', 'DESC')
				->join('movies m', 'm.ID = r.movie_id')
				->join('user_accounts ua', 'r.user_id = ua.id')
				->join('review_votes rv', 'r.id = rv.review_id', 'left')
				->group_by('r.id, r.created_date, ua.avatar_url, ua.name, ua.id, r.review_title, r.review_detail, r.rating')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result();
	}

	function getTotalReviewsByMovieId($movieid)
	{
		$this->db->select('count(*) as countReview, sum(r.rating) as totalReview')
				->from("$this->table r")
				->where('r.is_approved', 1)
				->where('r.movie_id', $movieid);
		
		$query = $this->db->get();
		return $query;
	}

	function getTotalReviewsByMovieIds($movieids)
	{
		$this->db->select('r.movie_id as MovieID, count(*) as countReview, sum(r.rating) as totalReview')
				->from("$this->table r")
				->where_in('r.movie_id', $movieids)
				->where('r.is_approved', 1)
				->group_by('r.movie_id');
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getById($reviewid, $userid = ""){
		$this->db->select("*")
				->from("$this->table")
				->where('id', $reviewid);

		if($userid != ""){
			$this->db->where('user_id', $userid)
					->where('is_approved', 1);
		}else{
			$this->db->where('is_approved != ', 2);
		}
				
		$query = $this->db->get();
		return $query;
	}

	//Backend functionality:

	public function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("r.*, u.name AS username, m.MovieName AS moviename")
				->from("$this->table r")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by("created_date", "desc");
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function set_filter($filter)
	{ 
		$status = element('isapproved', $filter, 1);
		$this->db->where('r.is_approved', $status);

		$isflag = element('isflag', $filter, -1);
		if($isflag != -1){
			$this->db->where('r.is_flag', $isflag);
		}

		$this->db->join('user_accounts u', 'r.user_id = u.id');
		$this->db->join('movies m', 'r.movie_id = m.ID');
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table r");
		$row =  $query->row();
		return $row->num;
	}

	function get_record_complete($id){
		$this->db->select("r.*, u.name AS username, m.MovieName AS moviename")
				->from("$this->table r")
				->where('r.id', $id)
				->join('user_accounts u', 'r.user_id = u.id')
				->join('movies m', 'r.movie_id = m.ID');

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return null;
	    }
	}

	function update_by_id($id, $data){
		$this->db->update('reviews', $data, "id = ".$id);
		return $this->db->affected_rows();
	}

}