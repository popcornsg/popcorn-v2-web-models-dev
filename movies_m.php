<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_m extends MY_Model{
	
	protected $table = 'movies';
	protected $primary_key = 'ID';
	protected $columns = array(
		'MovieName' => array('MovieName', 'required'),
		'MovieNameDisplay' => array('MovieNameDisplay', '', NULL, ""),
		'ListingImage' => array('ListingImage', 'trim|required'),
		'ThumbImage' => array('ThumbImage', 'trim'),
		'AnimatedImage' => array('AnimatedImage', 'trim'),
		'CountryCode' => array('CountryCode', 'trim|required'),
		'MasterID' => array('MasterID', 'trim|required'),
		'RunningTime' => array('RunningTime', 'trim|required'),
		'Reference' => array('Reference', 'trim'),
		'ReleaseDate' => array('ReleaseDate', 'trim|required'),
		'Language' => array('Language', 'trim|required'),
		'Genre' => array('Genre', 'trim|required'),
		'Distributor' => array('Distributor', 'trim|required'),
		'Synopsis' => array('Synopsis', 'trim|required'),
		'PDSynopsis' => array('PDSynopsis', 'trim'),
		'Trailer' => array('Trailer', 'trim'),
		'UpdateTrailer' => array('UpdateTrailer', 'trim'),
		'LocalCertification' => array('LocalCertification', 'trim|required'),
		'NowShowing' => array('NowShowing', 'trim'),
		'MovieStatus' => array('MovieStatus', 'trim|required'),
		'UpdateData' => array('UpdateData', 'trim'),
		'SpecialCase' => array('SpecialCase', 'trim'),
		'IsActive' => array('IsActive', 'trim'),
		'IsFilmFestival' => array('IsFilmFestival', 'trim')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	
	
	public function set_filter($filter)
	{
		//Active/ Inactive
		$status = element('status', $filter, 2);
		if($status !=2 ){
			$this->db->where('c.IsActive', $status);
		}

		//Now showing / Upcoming
		$moviestatus = element('movie_status', $filter, 2);
		if($moviestatus !=2 ){
			$this->db->where('c.NowShowing', $moviestatus);
		}

		//process not process:
		$process = element('process', $filter, 1);
		if($process !=2 ){
			$this->db->where('c.UpdateData', $process);
		}

		if($name = element('name', $filter)){
			$this->db->like('c.MovieName', $name);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('c.CountryCode', $country_code);
	}

	public function get_items($filter, $offset, $limit, $isFilmFestival = 0)
	{
		$this->set_filter($filter);
		$this->db->select("c.*")
				->from("$this->table c")
				->limit($limit, $offset);		

		if($isFilmFestival == 1){
			$this->db->where("IsFilmFestival", $isFilmFestival);
		}

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by("UpdateData", 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function get_items_by_term($countrycode, $term, $limit){
		$this->db->select("c.*")
				->from("$this->table c")
				->where('c.CountryCode', $countrycode)
				->like('c.MovieName', $term)
                                ->order_by("c.MovieName = '$term'", 'DESC')
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	public function get_mastermovie_by_term($term, $limit){
		$this->db->select("c.*")
				->from("master_movies c")
				->like('c.MasterName', $term)
                                ->order_by("c.MasterName = '$term'", 'DESC')
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_term($term, $limit){
		$this->db->select("c.*")
				->from("$this->table c")
				->like('c.MovieName', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	public function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	public function get_active_count()
	{
		$this->db->select('count(*) as num')
				->where('IsActive',1);
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	public function get_all_active_items($limit, $offset, $country_code)
	{
		$this->db->select("c.*")
				->from("$this->table c")
				->where('IsActive',1)
				->where('CountryCode', $country_code)
                                ->where('ReleaseDate <=', 'now()', false)
				->order_by('ReleaseDate', 'DESC')
				->limit($limit, $offset);		
		
		$query = $this->db->get();
		return $query->result();
	}

	public function getById($movieid, $countrycode = ""){
		$this->db->select("m.*, m.ID as MovieID, mm.Cast, mm.Director")
				->from("$this->table m")
				->join("master_movies mm", "m.MasterID = mm.ID")
				->where('m.ID', $movieid);

		if($countrycode != ""){
			$this->db->where('m.CountryCode', $countrycode);
		}

		$query = $this->db->get();
		return $query;
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	public function getEditRecord($movieid=0)
	{
		if($movieid){
			$this->db->select("m.*, mm.Cast, mm.MasterName, ml.LangCode, ml.Synopsis AS LangSynopsis, mm.Director, mm.ReviewIMDB")
				->from("$this->table m")
				->join("master_movies mm", "m.MasterID = mm.ID")
				->join("movie_langs ml", "m.ID = ml.MovieID", "LEFT")
				->where('m.ID', $movieid);

			$query = $this->db->get();
			if($query->num_rows()>0){
				return $query->row();
			}

			return false;			
		}

		return $this->get_empty_record();
	}

	public function getLatestByIds($movieidarr){
		$this->db->select("*")
				->from("$this->table")
				->where_in('ID', $movieidarr)
				->order_by('ReleaseDate', 'desc');

		$query = $this->db->get();
		return $query->result();
	}

	public function getLatestArrayByIds($movieidarr){
		$this->db->select("*")
				->from("$this->table")
				->where_in('ID', $movieidarr)
				->order_by('ReleaseDate', 'desc');

		$query = $this->db->get();
		return $query->result_array();
	}

	public function getByIds($movieidarr){
		$this->db->select("*")
				->from("$this->table")
				->where_in('ID', $movieidarr);

		$query = $this->db->get();
		return $query;
	}

	public function getCinemaMovieShowtime($showtimeid){
		$this->db->select("c.CinemaSName as CinemaName, m.MovieName, st.ShowTime")
				->from("showtimes st")
				->where('st.ID', $showtimeid)
				->join("cinemas c", "st.CinemaID = c.ID")
				->join("movies m", "st.MovieID = m.ID");
		
		$query = $this->db->get();
		return $query;
	}

	function update_lang($id, $langsynopsis){
		$this->db->update("movie_langs", array("Synopsis" => $langsynopsis), "MovieID = ".$id);
		return $this->db->affected_rows();
	}

	function insert_movie($data){
		if($this->db->insert("movies", $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}

	public function getNowShowingMovies($dynData, $attributes = array(), $refDate = array(), $countryCode = "") {
		$selectedCountryCode = "";
		if($countryCode != ""){
			$selectedCountryCode = $countryCode;
		}else{
			$selectedCountryCode = $this->COUNTRY;
		}

		//check lang
		if(strtolower($this->LANG) == LANG_EN){
			$this->db->select('m.ID as MovieID, m.MovieName, m.MovieNameDisplay, ListingImage, m.ThumbImage, m.AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, mt.counter as Counter, m.UpdateData, m.IsActive, m.ModifiedDate');
			$this->db->join('showtimes s', 's.MovieID = m.ID', 'LEFT');
			$this->db->join('cinemas c', 'c.ID = s.CinemaID', 'LEFT');
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
			$this->db->join('movie_total_views mt', 'mt.movie_id = m.ID', 'LEFT');
		}else{
			$this->db->select('m.ID as MovieID, m.MovieName, m.MovieNameDisplay, m.ListingImage, m.ThumbImage, m.AnimatedImage, m.Genre, m.Language, m.LocalCertification, m.RatingRTCritics, 
				m.RatingRTAudience, m.NowShowing, m.Synopsis, m.PDSynopsis, m.ReleaseDate, 
				m.RunningTime, mm.Director, mm.Cast, m.Trailer, mt.counter as Counter, 
				ml.MovieName as MovieNameLN, ml.RunningTime as RunningTimeLN, ml.Genre as GenreLN, ml.Synopsis as SynopsisLN, m.UpdateData, m.IsActive, m.ModifiedDate');
			$this->db->join('showtimes s', 's.MovieID = m.ID', 'LEFT');
			$this->db->join('cinemas c', 'c.ID = s.CinemaID', 'LEFT');
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
			$this->db->join('movie_total_views mt', 'mt.movie_id = m.ID', 'LEFT');
			$this->db->join('movie_langs ml', 'ml.MovieID = m.ID', 'LEFT');
			$this->db->where('ml.LangCode', strtolower($this->LANG));
		}

		//check cityID:
		if($this->CITYID != 0){
			$this->db->where('((m.IsFilmFestival = 1 AND m.NowShowing = 1) OR (s.ShowTime >= NOW() AND s.ShowTime <= DATE_ADD(CURRENT_DATE, INTERVAL 3 DAY) AND c.IsActive = 1 AND c.CityID = '.$this->CITYID.'))', NULL, false);
		}else{
			$this->db->where('((m.IsFilmFestival = 1 AND m.NowShowing = 1) OR (s.ShowTime >= NOW() AND s.ShowTime <= DATE_ADD(CURRENT_DATE, INTERVAL 3 DAY) AND c.IsActive = 1))', NULL, false);
		}


		$this->db->where('m.CountryCode', $selectedCountryCode);
		$this->db->where(array('m.IsActive' => '1'));
		$this->db->group_by('m.ID');
        $this->db->order_by('m.ID != 1502','ASC',false);
		$this->db->order_by('m.MovieName');
		$this->db->from('movies as m');
		$query = $this->db->get();

		$result = array();

		foreach ($query->result_array() as $m) {
			if ($m['RatingRTCritics'] == '-1' || $m['RatingRTCritics'] == '0') {
				$m['Rating'] 		= 0;
				$m['RottenRating']	= '';
			} else {
				$m['Rating']		= round ((int) $m['RatingRTAudience'] / 20);
				$m['RottenRating']	= 'Critics: '.$m['RatingRTCritics'].'% Audience: '.$m['RatingRTAudience'].'% (Rotten Tomatoes)';
			}
			
			$m['ReleaseDate'] = date('j F Y', strtotime($m['ReleaseDate']));
			
			unset($m['RatingRTCritics']);
			unset($m['RatingRTAudience']);
			
            if ($m['PDSynopsis'])
                $m['Synopsis'] = $m['PDSynopsis'];
		
			if (is_array($dynData)) {
				if (isset($dynData[((int) $m['MovieID'])])) {
					$m['CinemaIDs'] = $dynData[((int)$m['MovieID'])];
				} else {
					$m['CinemaIDs'] = array();
				}
			}

			//check lang:
			if(strtolower($this->LANG) != LANG_EN){
				$m['Synopsis'] = (isset($m['SynopsisLN']) && $m['SynopsisLN'] != "") ? $m['SynopsisLN'] : $m['Synopsis'];
				$m['MovieName'] = (isset($m['MovieNameLN']) && $m['MovieNameLN'] != "") ? $m['MovieNameLN'] : $m['MovieName'];
				$m['RunningTime'] = (isset($m['RunningTimeLN']) && $m['RunningTimeLN'] != "") ? $m['RunningTimeLN'] : $m['RunningTime'];
				$m['Genre'] = (isset($m['GenreLN']) && $m['GenreLN'] != "") ? $m['GenreLN'] : $m['Genre'];
			}

			$m['MovieNameOriginal'] = $m['MovieName'];

			//check name display:
			if(isset($m['MovieNameDisplay']) && $m['MovieNameDisplay'] != ""){
				$m['MovieName'] = $m['MovieNameDisplay'];
			}

			$m['Badge'] = "";
			$m['Overlay'] = "";

			//movie attributes:
			if(sizeof($attributes) > 0){
				$searchKey = array_search($m["MovieID"], array_column($attributes, "MovieID"));
				if($searchKey !== false){ 
					$platform = strtolower($this->platform);
					$defaultPlatform = "iphone";
					$badgeArr = json_decode($attributes[$searchKey]["Badge"], true);
					$overlayArr = json_decode($attributes[$searchKey]["PosterOverlay"], true);
					$badge = "";
					$overlay = "";
					if(is_array($badgeArr) && isset($badgeArr[$defaultPlatform."_3d"])){
						$badge = $badgeArr[$defaultPlatform."_3d"];
					}
					if(is_array($overlayArr) && isset($overlayArr[$platform."_3d"])){
						$overlay = $overlayArr[$platform."_3d"];
					}  

					$m['Badge'] = $badge;
					$m['Overlay'] = $overlay;
				}
			}

			//ref dates:
			$m["RefDays"] = array();
			if(isset($refDate[$m["MovieID"]])){
				$m["RefDays"] = $refDate[$m["MovieID"]];
			}
			
			if ($m['Synopsis'] && $m['ReleaseDate']) {
				$result[] = $m;
			}
		}
		
		return $result;
	}

	public function getUpcomingMovies($userID = false, $purchases = false, $limit = false, $backend = 0, $countryCode = "") {
		$selectedCountryCode = "";
		if($countryCode != ""){
			$selectedCountryCode = $countryCode;
		}else{
			$selectedCountryCode = $this->COUNTRY;
		}

        //CHECK LANGAUGE
		if(strtolower($this->LANG) == strtolower(LANG_EN)){
			$this->db->select('m.ID as MovieID, MovieName, MovieNameDisplay, ListingImage, ThumbImage, AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, m.UpdateData, m.IsActive, m.ModifiedDate');
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
		}else{
			$this->db->select('m.ID as MovieID, m.MovieName, m.MovieNameDisplay, m.ListingImage, m.ThumbImage, m.AnimatedImage, m.Genre, m.Language, m.LocalCertification, m.RatingRTCritics, 
					m.RatingRTAudience, m.NowShowing, m.Synopsis, m.PDSynopsis, m.ReleaseDate, m.RunningTime, mm.Director, mm.Cast, m.Trailer,
					ml.MovieName as MovieNameLN, ml.RunningTime as RunningTimeLN, ml.Genre as GenreLN, ml.Synopsis as SynopsisLN, m.UpdateData, m.IsActive, m.ModifiedDate');

			//JOIN WITH LANGUAGE TABLE:
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
			$this->db->join('movie_langs ml', 'ml.MovieID = m.ID', 'LEFT');
			$this->db->where('ml.LangCode', strtolower($this->LANG));
		}
		
		$this->db->where('m.CountryCode', $selectedCountryCode);
		$this->db->where(array('IsActive' => '1', 'NowShowing' => '0'));
                
        if ($limit !== false && is_numeric($limit)){
        	$this->db->limit($limit);
        }
            
		if($selectedCountryCode == "ID"){
			$where_au = "((ReleaseDate < DATE_ADD(NOW(), INTERVAL 30 DAY) AND ReleaseDate > DATE_SUB(NOW(), INTERVAL 5 DAY)) OR ReleaseDate = '0000-00-00 00:00:00')";
			$this->db->where($where_au);
		}else{
			$this->db->where('ReleaseDate <', 'DATE_ADD(NOW(), INTERVAL 30 DAY)', FALSE);
			$this->db->where('ReleaseDate >', 'DATE_SUB(NOW(), INTERVAL 5 DAY)', FALSE);
		}

		$this->db->group_by('MovieID');
		$this->db->order_by('ReleaseDate, MovieName');
		$this->db->from('movies as m');
		$query = $this->db->get();
		
		$result = array();
		foreach ($query->result_array() as $m) {
            if ($backend == 0 && ($m['ListingImage'] == '' || $m['ListingImage'] == 'https://s3-ap-southeast-1.amazonaws.com/popcornsg/placeholder-movieimage.png')) {
            	continue;
            }
   
			if ($m['RatingRTCritics'] == '-1' || $m['RatingRTCritics'] == '0') {
				$m['Rating'] 		= 0;
				$m['RottenRating']	= '';
			} else {
				$m['Rating']		= round ((int) $m['RatingRTAudience'] / 20);
				$m['RottenRating']	= 'Critics: '.$m['RatingRTCritics'].'% Audience: '.$m['RatingRTAudience'].'% (Rotten Tomatoes)';
			}
			
			if($m['ReleaseDate'] == "0000-00-00 00:00:00"){
				$m['ReleaseDate'] = "-";
			}else{
				$m['ReleaseDate'] = date('d F, Y', strtotime($m['ReleaseDate']));
			}
			
			$m['Notify'] = '0';
			unset($m['RatingRTCritics']);
			unset($m['RatingRTAudience']);
			unset($m['notified']);
                        
            if ($m['PDSynopsis']) {
            	$m['Synopsis'] = $m['PDSynopsis'];
            }
            unset($m['PDSynopsis']);

            $m['MovieNameOriginal'] = $m['MovieName'];
            
            //check lang:
			if(strtolower($this->LANG) != LANG_EN){
				$m['Synopsis'] = (isset($m['SynopsisLN']) && $m['SynopsisLN'] != "") ? $m['SynopsisLN'] : $m['Synopsis'];
				$m['MovieName'] = (isset($m['MovieNameLN']) && $m['MovieNameLN'] != "") ? $m['MovieNameLN'] : $m['MovieName'];
				$m['RunningTime'] = (isset($m['RunningTimeLN']) && $m['RunningTimeLN'] != "") ? $m['RunningTimeLN'] : $m['RunningTime'];
				$m['Genre'] = (isset($m['GenreLN']) && $m['GenreLN'] != "") ? $m['GenreLN'] : $m['Genre'];
			}

			//check name display:
			if(isset($m['MovieNameDisplay']) && $m['MovieNameDisplay'] != ""){
				$m['MovieName'] = $m['MovieNameDisplay'];
			}

			if ($m['Synopsis'] && $m['ReleaseDate']) {
				$result[] = $m;
			}
		}

		return $result;
	}

	public function getMovieDetail($movieid, $attributes = array(), $refDate = array()) {
		//check lang
		if(strtolower($this->LANG) == LANG_EN){
			if($this->user_id){
				$this->db->select('wl.status as isWatchList, m.ID as MovieID, m.MovieName, m.MovieNameDisplay, ListingImage, ThumbImage, AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, m.IsFilmFestival, m.Reference');
			}else{
				$this->db->select('m.ID as MovieID, m.MovieName, m.MovieNameDisplay, ListingImage, ThumbImage, AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, m.IsFilmFestival, m.Reference');
			}
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
			if($this->user_id){
				$this->db->join('watchlist wl', 'wl.movie_id = m.ID and wl.user_id = '.$this->user_id, 'LEFT');
			}
		}else{
			if($this->user_id){
				$this->db->select('wl.status as isWatchList, m.ID as MovieID, m.MovieName, m.MovieNameDisplay, m.ListingImage, m.ThumbImage, m.AnimatedImage, m.Genre, m.Language, m.LocalCertification, m.RatingRTCritics, 
					m.RatingRTAudience, m.NowShowing, m.Synopsis, m.PDSynopsis, m.ReleaseDate, 
					m.RunningTime, mm.Director, mm.Cast, m.Trailer, 
					ml.MovieName as MovieNameLN, ml.RunningTime as RunningTimeLN, ml.Genre as GenreLN, ml.Synopsis as SynopsisLN, m.IsFilmFestival, m.Reference');
			}else{
				$this->db->select('m.ID as MovieID, m.MovieName, m.MovieNameDisplay, m.ListingImage, m.ThumbImage, m.AnimatedImage, m.Genre, m.Language, m.LocalCertification, m.RatingRTCritics, 
					m.RatingRTAudience, m.NowShowing, m.Synopsis, m.PDSynopsis, m.ReleaseDate, 
					m.RunningTime, mm.Director, mm.Cast, m.Trailer, 
					ml.MovieName as MovieNameLN, ml.RunningTime as RunningTimeLN, ml.Genre as GenreLN, ml.Synopsis as SynopsisLN, m.IsFilmFestival, m.Reference');
			}
			$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
			$this->db->join('movie_langs ml', 'ml.MovieID = m.ID', 'LEFT');
			if($this->user_id){
				$this->db->join('watchlist wl', 'wl.movie_id = m.ID and wl.user_id = '.$this->user_id, 'LEFT');
			}
			$this->db->where('ml.LangCode', strtolower($this->LANG));
		}

		$this->db->where('m.CountryCode', $this->COUNTRY);
		$this->db->where(array('m.IsActive' => '1'));
		$this->db->where('m.ID', $movieid);
		$this->db->from('movies as m');
		$query = $this->db->get();

		$m = $query->row_array();
		if(sizeof($m) > 0){
			if ($m['RatingRTCritics'] == '-1' || $m['RatingRTCritics'] == '0') {
				$m['Rating'] 		= 0;
				$m['RottenRating']	= '';
			} else {
				$m['Rating']		= round ((int) $m['RatingRTAudience'] / 20);
				$m['RottenRating']	= 'Critics: '.$m['RatingRTCritics'].'% Audience: '.$m['RatingRTAudience'].'% (Rotten Tomatoes)';
			}
			
			$m['ReleaseDate'] = date('j F Y', strtotime($m['ReleaseDate']));
			
			unset($m['RatingRTCritics']);
			unset($m['RatingRTAudience']);
			
	        if ($m['PDSynopsis']){
	        	$m['Synopsis'] = $m['PDSynopsis'];
	        }

	        //check name display:
			if(isset($m['MovieNameDisplay']) && $m['MovieNameDisplay'] != ""){
				$m['MovieName'] = $m['MovieNameDisplay'];
			}
	            
			//check lang:
			if(strtolower($this->LANG) != LANG_EN){
				$m['Synopsis'] = (isset($m['SynopsisLN']) && $m['SynopsisLN'] != "") ? $m['SynopsisLN'] : $m['Synopsis'];
				$m['MovieName'] = (isset($m['MovieNameLN']) && $m['MovieNameLN'] != "") ? $m['MovieNameLN'] : $m['MovieName'];
				$m['RunningTime'] = (isset($m['RunningTimeLN']) && $m['RunningTimeLN'] != "") ? $m['RunningTimeLN'] : $m['RunningTime'];
				$m['Genre'] = (isset($m['GenreLN']) && $m['GenreLN'] != "") ? $m['GenreLN'] : $m['Genre'];
			}

			$m['Badge'] = "";
			$m['Overlay'] = "";

			//movie attributes:
			if(sizeof($attributes) > 0){
				$searchKey = array_search($m["MovieID"], array_column($attributes, "MovieID"));
				if($searchKey !== false){ 
					$platform = strtolower($this->platform);
					$defaultPlatform = "iphone";
					$badgeArr = json_decode($attributes[$searchKey]["Badge"], true);
					$overlayArr = json_decode($attributes[$searchKey]["PosterOverlay"], true);
					$badge = "";
					$overlay = "";
					if(is_array($badgeArr) && isset($badgeArr[$defaultPlatform."_3d"])){
						$badge = $badgeArr[$defaultPlatform."_3d"];
					}
					if(is_array($overlayArr) && isset($overlayArr[$platform."_3d"])){
						$overlay = $overlayArr[$platform."_3d"];
					}  

					$m['Badge'] = $badge;
					$m['Overlay'] = $overlay;
				}
			}

			//ref dates:
			$m["RefDays"] = array();
			if(isset($refDate[$m["MovieID"]])){
				$m["RefDays"] = $refDate[$m["MovieID"]];
			}
		}
		
		$result = $m;
		return $result;
	}

	public function getFilmFestival() {
		if ($this->user_id) {
			$this->db->select('m.ID as MovieID, MovieName, MovieNameDisplay, ListingImage, ThumbImage, AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, um.notified, m.UpdateData, m.IsActive');
			$this->db->join('user_account_movie_favs um', 'um.movie_id = m.ID and um.user_account_id = '.$this->user_id, 'left');
		} else {
			$this->db->select('m.ID as MovieID, MovieName, MovieNameDisplay, ListingImage, ThumbImage, AnimatedImage, Genre, Language, LocalCertification, RatingRTCritics, RatingRTAudience, NowShowing, Synopsis, PDSynopsis, ReleaseDate, RunningTime, mm.Director, mm.Cast, Trailer, m.UpdateData, m.IsActive');
		}

		$this->db->join('master_movies mm', 'm.MasterID = mm.ID', 'INNER');
		
		$this->db->where('m.CountryCode', $this->COUNTRY);
		$this->db->where(array('IsActive' => '1', 'IsFilmFestival' => '1'));
        
		$this->db->group_by('MovieID');
		$this->db->order_by('ReleaseDate, MovieName');
		$this->db->from('movies as m');
		$query = $this->db->get();
		
		$result = array();
		foreach ($query->result_array() as $m) {
			if ($m['RatingRTCritics'] == '-1' || $m['RatingRTCritics'] == '0') {
				$m['Rating'] 		= 0;
				$m['RottenRating']	= '';
			} else {
				$m['Rating']		= round ((int) $m['RatingRTAudience'] / 20);
				$m['RottenRating']	= 'Critics: '.$m['RatingRTCritics'].'% Audience: '.$m['RatingRTAudience'].'% (Rotten Tomatoes)';
			}
			
			if($m['ReleaseDate'] == "0000-00-00 00:00:00"){
				$m['ReleaseDate'] = "-";
			}else{
				$m['ReleaseDate'] = date('d F, Y', strtotime($m['ReleaseDate']));
			}
			
			unset($m['RatingRTCritics']);
			unset($m['RatingRTAudience']);
			unset($m['notified']);
                        
            if ($m['PDSynopsis']) {
            	$m['Synopsis'] = $m['PDSynopsis'];
            }
            unset($m['PDSynopsis']);

            //check lang:
			if(strtolower($this->LANG) != LANG_EN){
				$m['Synopsis'] = (isset($m['SynopsisLN']) && $m['SynopsisLN'] != "") ? $m['SynopsisLN'] : $m['Synopsis'];
				$m['MovieName'] = (isset($m['MovieNameLN']) && $m['MovieNameLN'] != "") ? $m['MovieNameLN'] : $m['MovieName'];
				$m['RunningTime'] = (isset($m['RunningTimeLN']) && $m['RunningTimeLN'] != "") ? $m['RunningTimeLN'] : $m['RunningTime'];
				$m['Genre'] = (isset($m['GenreLN']) && $m['GenreLN'] != "") ? $m['GenreLN'] : $m['Genre'];
			}

			//check name display:
			if(isset($m['MovieNameDisplay']) && $m['MovieNameDisplay'] != ""){
				$m['MovieName'] = $m['MovieNameDisplay'];
			}

			if ($m['Synopsis'] && $m['ReleaseDate']) {
				$result[] = $m;
			}
		}

		return $result;
	}
}
