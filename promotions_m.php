<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotions_m extends MY_Model{
	
	protected $table = 'promotion';
	protected $primary_key = 'id';
	protected $columns = array(
		'title' => array('Title', 'trim|required'),
		'merchant_id' => array('Merchant Id', 'trim|required'),
		'publish_start' => array('Publish Start', 'trim|required', NULL, 'now'),
		'publish_end' => array('Publish End', 'trim|required', NULL, '+1 week'),
		'start_date' => array('Start Date', 'trim|required',NULL, '+1 week'),
		'end_date' => array('End Date', 'trim|required', NULL, 'now'),
		'is_active' => array('Is Active', 'trim', NULL, 1),
		'image' => array('Image', 'trim|required'),
		'promotion_url' => array('Promotion Url', 'trim'),
		'intro' => array('Intro', 'trim'),
		'description' => array('Description', 'trim|required'),
		'discount_level' => array('Discount Level', 'trim|required|greater_than[0]|less_than[100]'),
	);	
	
	
	function set_filter($filter)
	{
		$status = element('status', $filter, 2);
		if($status !=2 ){
			$this->db->where('p.is_active', $status);
		}
		if($name = element('name', $filter)){
			$this->db->like('p.title', $name);
		}
		$this->db->join('merchant m', 'm.id=p.merchant_id', 'left');
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("p.*, m.name AS merchant")
				->from("$this->table p")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table p");
		$row =  $query->row();
		return $row->num;
	}


	function get_promo_categories($promo_id)
	{
		if(!$promo_id){ return array();}
			
		$this->db->select('c.name')	
				->from('promotion_category p')
				->join('category_dict c', 'c.id=p.dict_id')
				->where('p.promo_id', $promo_id);
		
		$query = $this->db->get('promotion_category');
		return $this->result_assoc_array($query, 'name');		
	}

	/**
	 * Save promotion categories in database
	 *	@param - integer category_id
	 *	@param - string list categories ex: cat1,cat2... 
	 */
	function save_categories($promo_id, $categories)
	{
		//delete old categories
		$this->delete_promo_categories($promo_id);
		
		if($categories)
		{			
			$categories = explode(',', $categories);
			//retrieve categories id
			$this->db->select('id, lower(name) AS name', FALSE) //select categories with lowercase
					->from('category_dict')
					->where_in('name', $categories);
			$query = $this->db->get();
			$db_categories = $this->result_assoc_array($query, 'name', 'id'); 	

			$to_insert = array();
			foreach($categories as $category)
			{
				$lower_name = strtolower($category); //we use all leters lowercase
				if(isset($db_categories[$lower_name])){
					$id = $db_categories[$lower_name];
				}
				else{
					$id = $this->db->insert('category_dict', array('name' => $category)); //insert new keyword
				}

				$to_insert[] = array('promo_id' => $promo_id, 'dict_id' => $id);			
			}

			if($to_insert){
				$this->db->insert_batch('promotion_category', $to_insert);
			}
		}
	}

	function delete_promo_categories($promo_id)
	{
		$this->db->where('promo_id', $promo_id)
			->delete('promotion_category');
	}



	/* ----------   MANAGE PROMO TYPE ----------------*/

	function get_promo_types($promo_id)
	{
		if(!$promo_id){ return array();}
			
		$this->db->select('c.name')	
				->from('promotion_type p')
				->join('type_dict c', 'c.id=p.dict_id')
				->where('p.promo_id', $promo_id);
		
		$query = $this->db->get('promotion_type');
		return $this->result_assoc_array($query, 'name');		
	}

	/**
	 * Save promotion types in database
	 *	@param - integer category_id
	 *	@param - string list types ex: type1,type2... 
	 */
	function save_types($promo_id, $types)
	{
		//delete old types
		$this->delete_promo_types($promo_id);
		
		if($types)
		{			
			$types = explode(',', $types);
			//retrieve types id
			$this->db->select('id, lower(name) AS name', FALSE) //select types with lowercase
					->from('type_dict')
					->where_in('name', $types);
			$query = $this->db->get();
			$db_types = $this->result_assoc_array($query, 'name', 'id'); 	

			$to_insert = array();
			foreach($types as $category)
			{
				$lower_name = strtolower($category); //we use all leters lowercase
				if(isset($db_types[$lower_name])){
					$id = $db_types[$lower_name];
				}
				else{
					$id = $this->db->insert('type_dict', array('name' => $category)); //insert new keyword
				}

				$to_insert[] = array('promo_id' => $promo_id, 'dict_id' => $id);			
			}

			if($to_insert){
				$this->db->insert_batch('promotion_type', $to_insert);
			}
		}
	}

	function delete_promo_types($promo_id)
	{
		$this->db->where('promo_id', $promo_id)
			->delete('promotion_type');
	}

	/* ------------- MANAGE PROMO PLATFORMS------*/

	/**
	 *  @param int promotion id
	 *	@param array of platform ids
	 */
	function save_platforms($promo_id, $platforms)
	{
		//delete old values
		$this->db->where('promo_id', $promo_id)->delete('promotion_platform');
		
		//insert new values
		if($platforms)
		{
			$to_insert = array();
			foreach($platforms as $dict_id)
			{
				$rec = array(
						'dict_id' => $dict_id,
						'promo_id' => $promo_id,
					);
				$to_insert[] = $rec;
			};
			$this->db->insert_batch('promotion_platform', $to_insert);
		}
	}

	function get_promo_platforms($promo_id)
	{
		if(!$promo_id){return array();}

		$query = $this->db->where('promo_id', $promo_id)->get('promotion_platform');
		return $this->result_assoc_array($query, 'dict_id');
	}



	/*** PROMOTION LOCATIONS **/

	function save_locations($promo_id, $locations)
	{
		//delete old values
		$this->db->where('promo_id', $promo_id)->delete('promotion_location');
		
		//insert new values
		if($locations)
		{
			$to_insert = array();
			foreach($locations as $index=> $dict_id)
			{
				$rec = array(
						'dict_id' => $dict_id,
						'promo_id' => $promo_id,
					);
				$to_insert[] = $rec;
			};
			$this->db->insert_batch('promotion_location', $to_insert);
		}
	}

	function get_promo_locations($promo_id)
	{
		if(!$promo_id){return array();}
		
		$query = $this->db->where('promo_id', $promo_id)->get('promotion_location');
		return $this->result_assoc_array($query, 'dict_id');
	}



	/*** PIKCY RESTAURANTS ***/
	function save_picky($promo_id, $restaurants)
	{
		//delete old values
		$this->db->where('promo_id', $promo_id)->delete('promotion_picky');

		//insert new 
		if($restaurants)
		{
			$data = array(
				'restaurant_names' => json_encode($restaurants),
				'restaurant_ids' => implode(',',  array_keys($restaurants)),
				'promo_id' => $promo_id,
			);
			$this->db->insert('promotion_picky', $data);
		}
	}	

	function get_picky_restaurants($promo_id)
	{	
		if(!$promo_id){return array();}

		$this->db->select('restaurant_names')
				->from('promotion_picky');
		$query = $this->db->get();
		if($query->num_rows()){
			$names = $query->row()->restaurant_names;
			return json_decode($names);
		}
		return array();
	}

        
	/* ------------- For use on Site/App(s) ------*/
        
        function get_promotions($promo_id = NULL, $type = 'all', $limit = 10, $offset = 0, $count = false) {
            $this->db->select('p.id as promo_id, p.title, p.intro, p.description, p.listing_image_thum as listing_image_thumb, p.image, p.start_date, p.end_date, p.discount_level, 
m.id as merchant_id, m.name as merchant_name, m.intro as merchant_intro, m.description as merchant_description, m.image_thumb as merchant_image_thumb, m.image as merchant_image');
            $this->db->join ('merchant m', 'm.id = p.merchant_id');
            $this->db->join ('promotion_platform pp', 'pp.promo_id = p.id');
            
            if ($type == 'all')
                $platform  = array(1,2,3);
            else if ($type == 'pocketdeals')
                $platform  = array(1);
            else if ($type == 'popcorn')
                $platform  = array(2);
            else if ($type == 'picky')
                $platform  = array(3);
            
            if ($promo_id === NULL) {
                $this->db->where(array('p.is_active' => 1, 'm.is_active' => 1));
                $this->db->where('p.publish_start <=', 'NOW()', false);
                $this->db->where('p.publish_end >=', 'NOW()', false);
                $this->db->where_in('pp.dict_id', $platform);

                $this->db->order_by('p.position');
                $this->db->order_by('p.discount_level', 'DESC');
            }
            else
                $this->db->where ('p.id', $promo_id);
            
            if (!$count)
                $this->db->limit($limit, $offset);
            
            $query      = $this->db->get('promotion p');
            
            if ($count)
                return $query->num_rows();
            
            if ($promo_id) {
                $promo  = $this->fix_promo_images($query->row());
                $promo->categories  = $this->get_promo_categories($promo_id);
                $promo->locations   = $this->get_promo_locations($promo_id);
                $promo->types       = $this->get_promo_types($promo_id);
                
                return $promo;
            }
            
            $promos     = array();
            
            foreach ($query->result() as $p)
                $promos[]   = $this->fix_promo_images($p);
            
            return $promos;
        }
        
        private function fix_promo_images($p) {
            if (!$p->listing_image_thumb && !empty($p->image))
                    $p->listing_image_thumb = $p->image;
            if (!$p->merchant_image_thumb && !empty($p->merchant_image))
                $p->merchant_image_thumb = $p->merchant_image;
            
            return $p;
        }
}
