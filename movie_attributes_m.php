<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_attributes_m extends MY_Model{

	protected $table = 'movie_attributes';
	protected $primary_key = 'ID';
	protected $columns = array(
		'MovieID' => array('Movie ID', 'trim|required'),
		'CinemaIDs' => array('Cinema ID\'s', 'trim|required'),
		'Badge' => array('Bagde Attribute', 'trim'),
		'PosterOverlay' => array('Overlay Attribute', 'trim')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function set_filter($filter)
	{ 
		$status = element('status', $filter, 2);
		if($status != 2){
			$this->db->where('b.IsActive', $status);
		}
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("b.*, m.MovieName")
				->from("$this->table b")
				->join('movies m', 'b.MovieID = m.ID')
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('CreatedDate', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("b.*")
				->from("$this->table b")
				->where('b.ID', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table b");
		$row =  $query->row();
		return $row->num;
	}

	function get_cinema_attributes($cinemaids){
		$array = explode(",", $cinemaids);
		$this->db->select("ID, CinemaName")
				->from("cinemas")
				->where_in('ID', $array);

		$query  = $this->db->get();
		return $query->result_array();
	}

	function getAllActiveAttributes(){
		$this->db->select("ID, MovieID, CinemaIDs, Badge, PosterOverlay, IsActive");
		$this->db->where('IsActive', 1);
		$this->db->from('movie_attributes');
		$query = $this->db->get();
		
		return $query->result_array();
	}

}