
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_accounts_activation_m extends MY_Model{
	
	protected $table = 'user_accounts_activation';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	

	public function insert_account_activation($data){
		return $this->db->insert($this->table, $data);
	}

	public function activate_user_account($userid){
		$data = array("status" => 0);
		$this->db->update($this->table, $data, "user_id = ".$userid);

		return true;
	}

	public function verify_user_account($userid){
		//verified user accounts table:
		$data = array("is_verified" => 1);
		$this->db->update("user_accounts", $data, "id = ".$userid);

		return true;
	}

	public function get_by_token($token, $userid){
		$this->db->select("*")
				->from($this->table)
				->where('token', $token)
				->where('user_id', $userid)
				->where('status', 1);

		$query = $this->db->get();
		return $query;
	}

}
