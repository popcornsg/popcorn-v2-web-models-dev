<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_usage_m extends MY_Model{

	protected $table = 'report_usages';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function getCountryReport($countrycode, $period)
	{ 
		$today = date('Y-m-d');

		$startdate = date('Y-m-d 00:00:00', strtotime('-1 day'));
		$enddate = date('Y-m-d 00:00:00', strtotime('-1 day'));
		if($period == 2){
			$startdate = date('Y-m-d 00:00:00', strtotime('-8 day'));
			$enddate = date('Y-m-d 00:00:00', strtotime('-1 day'));
		}else if($period == 3){
			$startdate = date('Y-m-d 00:00:00', strtotime('-31 day'));
			$enddate = date('Y-m-d 00:00:00', strtotime('-1 day'));
		}

		$sql = "Select platform, count(distinct UDID) as totalActiveUser from 
		report_usages where country_code = '".$countrycode."' 
		and logged >= '".$startdate."' and logged <= '".$enddate."' group by platform";
		if($period == 4){
			$sql = "Select platform, count(distinct UDID) as totalActiveUser from 
		report_usages where country_code = '".$countrycode."' 
		and logged <= '".$enddate."' group by platform";
		}
		
		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

}