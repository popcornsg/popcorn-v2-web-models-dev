<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_abbanner_m extends MY_Model{

	protected $table = 'report_ab_banner';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function getABReport($campaignid)
	{ 
		$sql = "SELECT ag.groupid, m.MovieName, m.ID, ra.banner_impression, ra.banner_clicks, ra.booking_clicks,
		ra.unique_booking_clicks, ra.purchase_clicks, ra.unique_purchase_clicks, ra.is_control from report_ab_banner ra join popcorn.ab_group_banners ag
		on ra.group_banner_id = ag.id
		join popcorn.movies m on ag.movieid = m.id
		where ag.campaign_id = ".$campaignid;

		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

	function getTotalSample($groupid){
		$sql = "select groupid, count(*) as TotalSample from popcorn.ab_users
		where groupid = ".$groupid." group by groupid";

		$query = $this->dbTracking->query($sql);
		if($query->num_rows() == 1){
			return $query->row()->TotalSample;
		}else{
			return 0;
		}
	}

	function getTotalControl(){
		$sql = "select count(*) as TotalSample from popcorn.ab_users_control";
		$query = $this->dbTracking->query($sql);
		if($query->num_rows() == 1){
			return $query->row()->TotalSample;
		}else{
			return 0;
		}
	}
}