<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Watchlist_m extends MY_Model{

	protected $table = 'watchlist';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function insertData($data){
		return $this->db->insert($this->table, $data);
	}

	public function getExistingWatchList($userid, $movieid){
		$this->db->select("*")
				->from("$this->table")
				->where('user_id', $userid)
				->where('movie_id', $movieid);

		$query = $this->db->get();
		return $query;
	}

	public function getUserWatchList($userid, $offset, $limit){
		$this->db->select("m.ID as MovieID, m.MovieName, m.MovieNameDisplay, m.ListingImage, m.AnimatedImage, m.Genre, m.Language, m.LocalCertification, m.RatingRTCritics, 
				m.RatingRTAudience, m.NowShowing, m.Synopsis, m.PDSynopsis, m.ReleaseDate, m.RunningTime, m.Trailer")
				->from("$this->table w")
				->where('w.user_id', $userid)
				->where('w.status', 1)
				->order_by('w.created_date', 'DESC')
				->join('movies m', 'm.ID = w.movie_id')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result_array();
	}

	function update_by_id($id, $data){
		$this->db->update("$this->table", $data, "id = ".$id);
		return $this->db->affected_rows();
	}
}