<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_view_m extends MY_Model{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	
	
	public function checkWasVisitor($articleid, $userip){
		$this->db->select("*")
				->from("article_views")
				->where('article_id', $articleid)
				->where('userip', $userip);

		$query = $this->db->get();
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function insert_article_view($data){
		if($this->db->insert("article_views", $data))
		{
			$this->db->select("*")
				->from("article_total_views")
				->where('article_id', $data['article_id']);

			$query = $this->db->get();
			if($query->num_rows() > 0){
				//update:
				$this->db->where('article_id', $data['article_id']);
				$this->db->set('counter', 'counter + 1', FALSE);
				$this->db->update('article_total_views');
			}else{
				$total["article_id"] = $data['article_id'];
				$total["counter"] = 1;
				$this->db->insert("article_total_views", $total);
			}

			return true;
		}else{
			return false;
		}
	}
}
