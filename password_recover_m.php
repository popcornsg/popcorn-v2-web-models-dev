
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password_recover_m extends MY_Model{
	
	protected $table = 'password_recovers';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	

	public function insert_password_recovers($data){
		return $this->db->insert($this->table, $data);
	}

	public function update_all_active($userid){
		$data = array("status" => 0);
		$this->db->update($this->table, $data, "user_id = ".$userid);
		return $this->db->affected_rows();
	}

	public function get_by_token($token){
		$this->db->select("*")
				->from($this->table)
				->where('token', $token)
				->where('status', 1);

		$query = $this->db->get();
		return $query;
	}

}
