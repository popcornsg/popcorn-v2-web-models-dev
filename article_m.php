<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_m extends MY_Model{

	/*
	*	Status:
	*   1 = Draft
	* 	2 = Publish
	* 	3 = Pending
	*/

	protected $table = 'articles';
	protected $primary_key = 'id';
	protected $columns = array(
		'title' => array('Title', 'trim|required'),
		'content' => array('Content', 'required'),
		'country_code' => array('Country', 'required'),
		'status' => array('Status', 'required'),
		'publishdate' => array('Publish Date', ''),
		'publishhour' => array('Publish Hour', ''),
		'publishminute' => array('Publish Minute', ''),
		'article_image' => array('Article Image', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}

	function set_filter($filter)
	{ 
		$status = element('status', $filter, 0);
		if($status != 0){
			$this->db->where('status', $status);
		}

		if($title = element('title', $filter)){
			$this->db->like('title', $title);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("*")
				->from("$this->table")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('id', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_pagination($offset, $limit, $country_code, $category = -1)
	{
        if ($category != -1 && $this->UDID && $this->platform){
        	$this->db->join("article_categories ac", "ac.article_id = a.id")
        			->join("categories cr", "cr.id = ac.category_id");

        	if($category != 0){
        		$this->db->where('ac.category_id', $category);
        	}else{
        		$this->db->where('cr.category_type', 1);
        	}
            $this->db->group_by('a.id');

			$this->db->select("a.*, cr.category")
					->from("$this->table a")
					->where('a.status', 2)
					->where('a.country_code', $country_code)
					->order_by('a.published_date', 'DESC')
					->limit($limit, $offset);
        }else{
        	$this->db->select("a.*")
					->from("$this->table a")
					->where('a.status', 2)
					->where('a.country_code', $country_code)
					->order_by('a.published_date', 'DESC')
					->limit($limit, $offset);
        }
            
		$query = $this->db->get();
		return $query->result();
	}

	function get_pagination_movie($offset, $limit, $country_code, $movieid)
	{
		$this->db->join('articles ar', 'am.articleid = ar.id')
					->join("article_categories ac", "ac.article_id = ar.id")
        			->join("categories cr", "cr.id = ac.category_id");

        $this->db->group_by('ar.id');

		$this->db->select("ar.*, cr.category")
				->from("article_movies am")
				->where('am.movieid', $movieid)
				->where('ar.status', 2)
				->where('ar.country_code', $country_code)
				->order_by('ar.published_date', 'DESC')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result();
	}

	function get_trending_articles(){
		$this->db->select("a.*, cr.category, IF(av.counter IS NULL, 0, av.counter) as counter", false)
				->from("articles a")
				->join("article_categories ac", "ac.article_id = a.id")
        		->join("categories cr", "cr.id = ac.category_id")
				->join('article_total_views av', 'a.id = av.article_id', 'left')
				->where('cr.category_type', 1)
				->group_by('a.id')
				->limit(10, 0)
				->order_by("counter", "DESC");

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("a.*")
				->from("$this->table a")
				->where('a.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_article_tags($articleid){
		$this->db->select("ac.*")
				->from("article_categories ac")
				->join('categories c', 'c.id = ac.category_id')
				->where('c.category_type', 2)
				->where('ac.article_id', $articleid);

		$query = $this->db->get();
		$tags = array();
		foreach ($query->result() as $v) {
			$tags[] = $v->category_id;
		}

		return $tags;
	}

	function get_related_articles($tags, $articleid){
		if($tags && sizeof($tags) > 0){
			$this->db->select("distinct a.id, a.title, a.content, a.article_image", false)
			->from("articles a")
			->join('article_categories ac', 'a.id = ac.article_id')
			->where_in('ac.category_id', $tags)
			->where('a.id !=', $articleid)
			->limit(5, 0);

			$query = $this->db->get();
			return $query->result();
		}else{
			return array();
		}	
	}

	function get_article_movies($articleid){
		$this->db->select("a.*, m.MovieName as moviename, m.MovieNameDisplay, m.ID as movieid, m.ListingImage as imagemovie")
				->from("article_movies a")
				->where('a.articleid', $articleid)
				->join('movies m', 'a.movieid = m.ID', 'left');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
	        return $query->result();
	    } else {
	    	return null;
	    }
	}

	function get_article_categories($articleid){
		$this->db->select("a.*, c.category, c.category_type")
				->from("article_categories a")
				->where('a.article_id', $articleid)
				->join('categories c', 'a.category_id = c.id');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
	        return $query->result();
	    } else {
	    	return null;
	    }
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	function insert_article_movie($data){
		return $this->db->insert_batch('article_movies', $data);
	}

	function insert_article_category($data){
		return $this->db->insert_batch('article_categories', $data);
	}

	function insert_article($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}

	function update_article($id, $data){
		$this->db->update('articles', $data, "id = ".$id);
		$this->success[] = "Updated successfully";
		return $this->db->affected_rows();
	}

	function delete_article_movie($articleid, $movieid){
		return $this->db->delete('article_movies', array('articleid' => $articleid, 'movieid' => $movieid)); 
	}

	function delete_article_category($articleid, $catid){
		return $this->db->delete('article_categories', array('article_id' => $articleid, 'category_id' => $catid)); 
	}

	function delete_article($articleid){
		return $this->db->delete('article_movies', array('articleid' => $articleid)); 
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table");
		$row =  $query->row();
		return $row->num;
	}

	function getById($newsid){
		$this->db->select("a.*, cr.category, ad.id as admin_id, ad.first_name")
				->from("$this->table a")
				->join("admin ad", "ad.id = a.adminid")
				->join("article_categories ac", "ac.article_id = a.id")
        		->join("categories cr", "cr.id = ac.category_id and cr.category_type = 1")
				->where('a.status', 2)
				->where('a.id', $newsid)
				->group_by('a.id');

		$query = $this->db->get();
		return $query;
	}

	function getByIds($newsids){
		$this->db->select("*")
				->from("$this->table")
				->where('status', 2)
				->where_in('id', $newsids);

		$query = $this->db->get();
		return $query;
	}

	function get_articles_by_movieid($movieid){
		$this->db->select("ar.*")
				->from("article_movies am")
				->where('am.movieid', $movieid)
				->where('ar.status', 2)
				->order_by('ar.published_date', 'DESC')
				->join('articles ar', 'am.articleid = ar.id')
				->limit(4, 0);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
	        return $query->result();
	    }else{
	    	return null;
	    }
	}

	function get_latest_articles($country_code){
		//TODO: BLOG
		$country_code = "SG";
		$this->db->select("ar.*")
				->from("articles ar")
				->where('ar.status', 2)
				->where('ar.country_code', $country_code)
				->order_by('ar.published_date', 'DESC')
				->limit(4, 0);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
	        return $query->result();
	    }else{
	    	return array();
	    }
	}

	function get_items_by_term($countrycode, $term, $limit){
		$this->db->select("c.*")
				->from("$this->table c")
				->where('c.country_code', $countrycode)
				->like('c.title', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	function get_all_scheduled(){
		$this->db->select("ar.*")
				->from("articles ar")
				->where('ar.status', 3)
				->order_by('ar.published_date', 'DESC');

		$query = $this->db->get();
		return $query;
	}

	function get_category_by_term($term, $type, $limit){
		$this->db->select("c.*")
				->from("categories c")
				->where("c.category_type", $type)
				->like('c.category', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	function insert_category($data){
		if($this->db->insert("categories", $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			return $id;
		}else{
			return false;
		}
	}
}