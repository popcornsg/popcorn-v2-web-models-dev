<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_m extends MY_Model{

	protected $table = 'newsletters';
	protected $primary_key = 'id';
	protected $columns = array(
		'subject' => array('Email Subject', 'trim|required'),
		'email_template' => array('Email template', 'trim|required', NULL, "0"),
		'substitutes' => array('Data subtitution', 'trim', NULL, ""),
		'country_code' => array('Country', 'trim|required'),
		'substitutes' => array('Banner Type', 'trim', NULL, 3),
		'content' => array('Email Content', 'trim'),
		'status' => array('Email Status', 'trim', NULL, ""),
		'scheduleddate' => array('Scheduled Date', ''),
		'scheduledhour' => array('Scheduled Hour', ''),
		'scheduledminute' => array('Scheduled Minute', ''),
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}

	function set_filter($filter)
	{ 
		$status = element('status', $filter, 0);
		if($status != 0){
			$this->db->where('status', $status);
		}

		if($subject = element('subject', $filter)){
			$this->db->like('subject', $subject);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("*")
				->from("$this->table")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('id', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("a.*")
				->from("$this->table a")
				->where('a.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table");
		$row =  $query->row();
		return $row->num;
	}

	function getById($newsid, $status = 2){
		if($status == 0){
			$this->db->select("*")
				->from("$this->table")
				->where('id', $newsid);
		}else{
			$this->db->select("*")
				->from("$this->table")
				->where('status', $status)
				->where('id', $newsid);
		}

		$query = $this->db->get();
		return $query;
	}

	function insert_newsletter($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}

	function update_newsletter($id, $data){
		$this->db->update('newsletters', $data, "id = ".$id);
		$this->success[] = "Updated successfully";
		return $this->db->affected_rows();
	}

}