<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscriber_m extends MY_Model
{
	protected $table = 'subscribers';

	public function __construct()
	{
		parent::__construct();	
	}

	public function checkInsert($email, $countrycode){
		$existing = $this->db->select("*")
				->from($this->table)
				->where('email', $email);

		$query = $this->db->get();
		if($query->num_rows() == 0){
			$data = array(
				"email" => $email,
				"country_code" => $countrycode
			);
			
			if($this->db->insert($this->table, $data)){
				$id = $this->db->insert_id();
				if(!$id) $id = false;
				return $id;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
}
?>