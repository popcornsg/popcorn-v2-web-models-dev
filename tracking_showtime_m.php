<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tracking_showtime_m extends MY_Model{

	protected $table = 'tracking_showtimes';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function getPopcornPurchaseByMovies($movieidarr, $startdate, $enddate)
	{ 
		$this->dbTracking->_protect_identifiers = false;	
		$this->dbTracking->select('CinemaID, MovieID, UDID, 1 as TotalPopcorn')
			->where('CreatedDate >', $startdate)
			->where('CreatedDate <', $enddate)
			->where_in('MovieID', $movieidarr);

		$query = $this->dbTracking->get('report_online_purchases');
		return $query->result_array();
	}

	function getPopcornPurchaseByMovieCinemaGroup($movieid, $startdate, $enddate)
	{ 
		$this->dbTracking->_protect_identifiers = false;	
		$this->dbTracking->select('CinemaID, GroupID, MovieID, UDID, 1 as TotalPopcorn')
			->where('CreatedDate >', $startdate)
			->where('CreatedDate <', $enddate)
			->where('MovieID', $movieid);

		$query = $this->dbTracking->get('report_online_purchases');
		return $query->result_array();
	}

	function getPopcornPurchaseByMovieCinema($movieid, $cinemaidarr, $startdate, $enddate)
	{ 
		$this->dbTracking->_protect_identifiers = false;
		$this->dbTracking->select('CinemaID, MovieID, UDID, 1 as TotalPopcorn')
			->where('CreatedDate >', $startdate)
			->where('CreatedDate <', $enddate)
			->where('MovieID', $movieid)
			->where_in('CinemaID', $cinemaidarr);

		$query = $this->dbTracking->get('report_online_purchases');
		return $query->result_array();
	}

	function getShowtimesByMovies($startdate, $enddate, $countrycode = "SG")
	{ 
		$this->dbTracking->select('ts.MovieID, SUM(ts.Available) as TotalAvailable, SUM(ts.Unavailable) as TotalUnavailable
, SUM(ts.TotalSeats) as TotalSeats')
			->where('ts.ShowTime >', $startdate)
			->where('ts.ShowTime <', $enddate)
			->where('cm.Country', $countrycode)
			->join('cinema_group_map cm', 'cm.CinemaID = ts.CinemaID')
			->group_by(array('ts.MovieID'))
			->order_by('TotalSeats','desc');

		$query = $this->dbTracking->get('tracking_showtimes ts');
		return $query->result_array();
	}

	function getShowtimesByCinemaGroups($movieid, $startdate, $enddate)
	{ 
		$this->dbTracking->select('cm.GroupID, SUM(Available) as TotalAvailable, SUM(Unavailable) as TotalUnavailable
, SUM(TotalSeats) as TotalSeats')
			->where('ShowTime >', $startdate)
			->where('ShowTime <', $enddate)
			->where('MovieID', $movieid)
			->join('cinema_group_map cm', 'cm.CinemaID = ts.CinemaID')
			->group_by(array('cm.GroupID')); 

		$query = $this->dbTracking->get('tracking_showtimes ts');
		return $query->result_array();
	}

	function getShowtimesByCinemasMovie($movieid, $groupid, $startdate, $enddate)
	{ 
		$this->dbTracking->select('ts.CinemaID, SUM(Available) as TotalAvailable, SUM(Unavailable) as TotalUnavailable
, SUM(TotalSeats) as TotalSeats')
			->where('ShowTime >',$startdate)
			->where('ShowTime <',$enddate)
			->where('MovieID', $movieid)
			->where('cm.GroupID', $groupid)
			->join('cinema_group_map cm', 'cm.CinemaID = ts.CinemaID')
			->group_by(array('cm.CinemaID')); 

		$query = $this->dbTracking->get('tracking_showtimes ts');
		return $query->result_array();
	}

	function getShowtimesByHour($startdate, $enddate, $countrycode = 'SG')
	{ 
		$this->dbTracking->select('HOUR(ts.ShowTime) as ShowTimeOnly, SUM(ts.Unavailable) as TotalUnavailable, SUM(ts.TotalSeats) as TotalSeats')
			->where('ts.ShowTime >', $startdate)
			->where('ts.ShowTime <', $enddate)
			->where('cm.Country', $countrycode)
			->join('cinema_group_map cm', 'cm.CinemaID = ts.CinemaID')
			->group_by(array('HOUR(ts.ShowTime)')); 

		$query = $this->dbTracking->get('tracking_showtimes ts');
		return $query->result_array();
	}

	function getShowtimesByMovieAndReleaseDate($movieid, $releasedate)
	{ 
		//$date = strtotime("+15 days", strtotime($releasedate));
		$finalDate = date("Y-m-d h:i:s");
		
		$this->dbTracking->_protect_identifiers = FALSE;
		$this->dbTracking->select('DATE_FORMAT(ShowTime, "%e %b") as DOM, SUM(Available) as TotalAvailable, SUM(Unavailable) as TotalUnavailable
, SUM(TotalSeats) as TotalSeats')
			->where('ShowTime >', $releasedate)
			->where('ShowTime <', $finalDate)
			->where('MovieID', $movieid)
			->group_by(array('DOM'))
			->order_by('ShowTime', 'asc'); 

		$query = $this->dbTracking->get('tracking_showtimes');
		return $query->result_array();
	}

	function getUserIDsByMovie($movieid, $fromdate, $todate)
	{ 
		$this->dbTracking->select('UserID, UDID')
			->where('CreatedDate >', $fromdate)
			->where('CreatedDate <', $todate)
			->where('MovieID', $movieid)
			->where('UserID IS NOT NULL', null)
			->group_by(array('UserID', 'UDID'));

		$query = $this->dbTracking->get('tracking_bookings');
		return $query->result_array();
	}

	function getUDIDByMovie($movieid, $fromdate, $todate)
	{ 
		$this->dbTracking->select('UserID, UDID')
			->where('CreatedDate >', $fromdate)
			->where('CreatedDate <', $todate)
			->where('MovieID', $movieid)
			->group_by(array('UserID', 'UDID'));

		$query = $this->dbTracking->get('tracking_bookings');
		return $query->result_array();
	}

	function getTopPastMovieClickByUDID($movieid, $udids, $startdate)
	{ 
		//get past 3 months data
		$months = array();
		$years = array();
		for($i = 0;$i <= 29;$i++){
			if($i == 0){
				$months[] = date('m');
				$years[] = date('Y');
			}else{
				$months[] = date('m', strtotime($startdate.' -'.$i.' month'));
				$years[] = date('Y', strtotime($startdate.' -'.$i.' month'));
			}
		}

		$this->dbTracking->select('MovieID, count(distinct UDID) as Click')
			->where_in('UDID', $udids)
			->where_in('Year', $years)
			->where_in('Month', $months)
			->where('MovieID !=', $movieid)
			->group_by(array('MovieID'))
			->order_by('Click', 'DESC')
			->limit(20, 0);

		$query = $this->dbTracking->get('tracking_users_movies');

		return $query->result_array();
	}

	//TODO: Review for all campaign
	function deleteWebUserMap($movieid){
		//delete
		//$sql = "delete from web_user_map where movie_id = ".$movieid;
		//$query = $this->dbTracking->query($sql);
	}

	//TODO: Review for all campaign
	function updateWebUserMap($movieid, $source){
		//insert
		//$sql = "insert into web_user_map(app_udid, movie_id, udid, source)
//select distinct wu.app_user_id, wu.movie_id, wu.udid, '".$source."' from popcorn.web_raw_logs wr
//join popcorn.web_users wu on wr.udid = wu.udid where wr.url like '%rogueone' and wr.referer like '%".$source."%' and wu.movie_id = ".$movieid;
		//$query = $this->dbTracking->query($sql);
	}

	function getClickShowTimeWebUser($from, $to, $movieid, $source){
		$dateFrom = date($from." h:i:s");
		$dateTo = date($to." h:i:s");

		$sql = "select count(distinct tb.UDID) as Total from tracking_bookings tb join web_user_map wu 
on tb.UDID = wu.udid where tb.MovieID = ".$movieid." and tb.Platform = 'Website' 
and wu.source = '".$source."' and tb.CreatedDate > '".$dateFrom."' and tb.CreatedDate < '".$dateTo."'";
		$query = $this->dbTracking->query($sql);
		return $query->row();
	}

	function getClickShowTimeAppUser($from, $to, $movieid, $source){
		$dateFrom = date($from." h:i:s");
		$dateTo = date($to." h:i:s");

		$sql = "select count(tb.UDID) as Total from tracking_bookings tb join web_user_map wu 
on tb.UDID = wu.app_udid where tb.MovieID = ".$movieid." 
and wu.source = '".$source."' and tb.CreatedDate > '".$dateFrom."' and tb.CreatedDate < '".$dateTo."'";
		$query = $this->dbTracking->query($sql);
		return $query->row();
	}

	function getTotalSoldFromShowtime($startdate, $enddate, $countrycode){
		$sql = "SELECT ts.MovieID, Sum(Unavailable) as TotalSold from tracking_showtimes ts
		join popcorn.cinemas c on ts.CinemaID = c.ID
		where ts.ShowTime > '".$startdate."'
		and ts.ShowTime < '".$enddate."'
		and c.GroupID in (select ID from popcorn.cinema_groups where CountryCode = '".$countrycode."')
		group by ts.MovieID
		order by TotalSold Desc
		limit 15";
		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

	function getTotalSoldFromOnlinePopcorn($startdate, $enddate, $countrycode){
		$sql = "SELECT MovieID, count(UDID) * 2.5 as TotalSold from report_online_purchases ro
		JOIN popcorn.cinema_groups cg on ro.GroupID = cg.ID
		where CreatedDate > '".$startdate."'
		and CreatedDate < '".$enddate."'
		and cg.CountryCode = '".$countrycode."'
		group by MovieID
		order by TotalSold Desc";

		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

	function getTotalSoldFromOfflinePopcorn($startdate, $enddate){
		$sql = "SELECT tb.MovieID, tof.UDID, count(distinct DATE_FORMAT(tof.createddate, '%Y-%m-%d %H:00:00')) as TotalSold from tracking_offline tof
		join tracking_bookings tb
		on tof.CinemaID = tb.CinemaID and tof.UDID = tb.UDID
		where tof.createddate > tb.CreatedDate
		and tb.ShowTime != '0000-00-00 00:00:00' and tb.platform != 'Website' and
		tof.createddate > '".$startdate."'
		and tof.createddate < '".$enddate."'
		and TIMESTAMPDIFF(HOUR, tof.createddate, tb.ShowTime) >=0 and TIMESTAMPDIFF(HOUR, tof.createddate, tb.ShowTime) <= 3
		group by tb.MovieID, tof.UDID";

		$query = $this->dbTracking->query($sql);
		return $query->result_array();
	}

}