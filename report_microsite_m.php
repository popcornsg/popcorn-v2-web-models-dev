<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_microsite_m extends MY_Model{

	protected $table = 'report_microsites';

	public function __construct()
	{
		parent::__construct();
		$this->dbTracking = $this->load->database('tracking', TRUE);
		$this->dbTracking->_protect_identifiers = true;	
	}

	function getCampaign(){
		$this->dbTracking->select("*")
				->from("report_microsites_campaign");

		$query = $this->dbTracking->get();
		return $query->result();
	}

	function getReport($campaigns)
	{ 
		$this->dbTracking->select("*")
				->from("report_microsites_campaign")
				->where("campaign", $campaigns);
		$query = $this->dbTracking->get();

		if($query->num_rows() == 1){
			$campaign_map = $query->row()->campaign_map;
			$maps = explode(",", $campaign_map);

			$finalResult = array();
			foreach ($maps as $m) {
				$raw = explode("-", $m);
				$movieid = $raw[1];
				$countrycode = $raw[0];

				$sql = "SELECT source, sum(totaltraffic) as totaltraffic, sum(totalinteractions) as totalinteractions, 
		sum(totalappdownload_click) as totalappdownload_click, sum(totalapptraffic_android) as totalapptraffic_android, sum(totalapptraffic_ios) as totalapptraffic_ios,
		sum(totalshowtimeclick_web) as totalshowtimeclick_web, sum(totalshowtimeclick_app) as totalshowtimeclick_app from report_microsites where
		movie_id = ".$movieid." GROUP BY source";

				$query = $this->dbTracking->query($sql);
				$finalResult[] = array(
					"country_code" => $countrycode,
					"summary" => $query->result_array()
				);
			}

			return $finalResult;
		}else{
			return array();
		}
	}
}