<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Showtimes_m extends MY_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);;
	}
	
	public function getDynamicData($movieids = array(), $cinemaids = array(), $countryCode = ""){
		$selectedCountryCode = "";
		if($countryCode != ""){
			$selectedCountryCode = $countryCode;
		}else{
			$selectedCountryCode = $this->COUNTRY;
		}

		$this->db->select('CinemaID, MovieID, DATE_FORMAT(showtime, "%Y-%m-%d") as DT', false);
		$this->db->join('cinemas', 'cinemas.ID = CinemaID');
		$this->db->join('movies', 'movies.ID = MovieID');
		$this->db->join('cinema_groups', 'cinema_groups.ID = cinemas.GroupID');
		
		$this->db->group_by(array('CinemaID', 'MovieID', 'DT'));
		$this->db->where('cinema_groups.CountryCode', $selectedCountryCode);
		$this->db->where('showtime >', 'NOW()', FALSE);
		$this->db->where('showtime <=', 'DATE_ADD(NOW(), INTERVAL 30 DAY)', FALSE);
		
		//check cityID:
		if($this->CITYID != 0){
			$this->db->where('cinemas.CityID', $this->CITYID);
		}

		if(sizeof($movieids) > 0){
			$this->db->where_in('movies.ID', $movieids);
		}

		$this->db->where('movies.IsFilmFestival', 0);

		if(sizeof($cinemaids) > 0){
			$this->db->where_in('cinemas.ID', $cinemaids);
		}

		$this->db->order_by('cinemas.CinemaName');
		$this->db->order_by('movies.MovieName');
		$this->db->order_by('DT');
		$query	= $this->db->get('showtimes');
		
		$cList	= array();
		$mList	= array();
		$mRefList = array();
		$cRefList = array();
		$result = $query->result();
		
		foreach ($result as $r) {
			if(isset($cList[$r->CinemaID])){
				if (!in_array($r->MovieID, $cList[$r->CinemaID])){
	            	$cList[$r->CinemaID][]	= $r->MovieID;
	            }
			}else{
				$cList[$r->CinemaID][]	= $r->MovieID;
			}
		}
			
		foreach ($result as $r) {
			if(isset($mList[$r->MovieID])){
				if (!in_array($r->CinemaID, $mList[$r->MovieID])){
	            	$mList[$r->MovieID][] = $r->CinemaID;
	            }
			}else{
				$mList[$r->MovieID][] = $r->CinemaID;
			}
		}

		//get ref day for movie:
		foreach ($mList as $key => $val) {
			$tempList = array();
			foreach ($result as $r) {
				if($key == $r->MovieID){
					$search = array_search($r->DT, $tempList);
					if($search === false){
						$tempList[] = $r->DT;
					}
				}
			}
			$mRefList[$key] = $tempList;
		}

		//get ref day for cinema:
		foreach ($cList as $key => $val) {
			$tempList = array();
			foreach ($result as $r) {
				if($key == $r->CinemaID){
					$search = array_search($r->DT, $tempList);
					if($search === false){
						$tempList[] = $r->DT;
					}
				}
			}
			$cRefList[$key] = $tempList;
		}

		return array('Cinemas' => $cList, 'Movies' => $mList, 'MovieRefDay' => $mRefList, 'CinemaRefDay' => $cRefList);
	}

	public function getShowtimes($hidemovieblock = 0, $refdays = array(), $mIDs = array(), $cIDs = array(), $udid = -1, $lang = LANG_EN, $attributes = array(), $countryCode = "") {
		$selectedCountryCode = "";
		if($countryCode != ""){
			$selectedCountryCode = $countryCode;
		}else{
			$selectedCountryCode = $this->COUNTRY;
		}

		//check valid refdays:
		if(sizeof($refdays) < 1){
			return false;
		}

		$this->db->select("s.ID as ShowTimeID, DATE_FORMAT(ShowTime, '%Y-%m-%d') as ShowDate, ShowTime, CinemaID, MovieID, ShowType, URL, Percentage, Available, 
			Unavailable, c.CinemaName, c.CinemaSName, m.ListingImage, m.MovieName, m.MovieNameDisplay, m.Genre, m.Language, m.RunningTime, m.LocalCertification, c.GroupID", false);
		$this->db->join('cinemas c', 'c.ID = s.CinemaID');
		$this->db->join('movies m', 'm.ID = s.MovieID');
		$this->db->where('m.IsActive', 1);
		$this->db->where('m.CountryCode', $selectedCountryCode);
		$this->db->where('c.IsActive', 1);
        $this->db->order_by('MovieID != 1502','ASC',false);
        $this->db->order_by('CinemaID != 801','ASC',false);
        $this->db->order_by('CinemaID != 802','ASC',false);
        $this->db->order_by('CinemaID != 803','ASC',false);
		$this->db->order_by('c.CinemaName');
		
		//check cityID:
		if($this->CITYID != 0){
			$this->db->where('c.CityID', $this->CITYID);
		}
		
		//check cinemaid
		if (sizeof($cIDs) > 0) {
			$this->db->where_in('s.CinemaID', $cIDs);
		}
		
		//check movieid
		if (sizeof($mIDs) > 0) {
			$this->db->where_in('s.MovieID', $mIDs);
		}
		
		$baseDate = date('Y-m-d', strtotime($refdays[0]));
        $now_date = date('Y-m-d');

        if($now_date == $baseDate){
        	$this->db->where('s.ShowTime >=', 'NOW()', FALSE);
        }else{
        	$this->db->where('s.ShowTime >=', date('Y-m-d 00:00:00', strtotime($refdays[0])));
        }

		$this->db->where('s.ShowTime <=', date('Y-m-d 23:59:59', strtotime($refdays[sizeof($refdays) - 1])));
		$this->db->order_by('ShowDate, m.MovieName, s.ShowTime');
		
		$query = $this->db->get('showtimes s');
		if ($query->num_rows() == 0) {
			return false;
		}

		$result	= array();
		$return = array();
		$result_arr = $query->result_array();
		$movies = array();

		foreach ($result_arr as $r) {
			if($r['ShowType'] == '3D') {
				$r['ShowType'] = ' (3D)';
			}
				
			if($r['ShowType'] == 'Digital' || $r['ShowType'] == 'Dig' ||  strpos(strtolower($r['ShowType']), strtolower("Dolby Digital")) !== false) {
				$r['ShowType'] = ' (2D)';
			}

			if($r['ShowType'] == 'Dolby Atmos Digital') {
				$r['ShowType'] = ' (Atmos)';
			}

			if($r['ShowType'] == 'First Class') {
				$r['ShowType'] = ' (First Class)';
			}
			
			$showtime_ori = $r['ShowTime'];
			$r['ShowTime'] = date('h:iA', strtotime($r['ShowTime']));

			$postfix = "";
			if(strpos(strtolower($r['ShowType']), "imax") !== false && strpos(strtolower($r['ShowType']), "3d") !== false){
				$postfix = "_3d";
			}

			$r['SeatStatus'] = $this->seat_status($r['Percentage']);
			$r['Notify'] = "0";

			unset($r['Percentage']);
			$keyMovie = $r['MovieID']; //key always MovieID
			if($countryCode != ""){
				$keyCinema = $r['CinemaSName']."__".$r['CinemaID']; //key always MovieID
			}else{
				$keyCinema = $r['CinemaName']."__".$r['CinemaID']; //key always MovieID
			}
			

			//change URL format:
			//replace domain URL:
			$baseUrl = preg_replace("/(http:\/\/api.popcorn.sg\/|http:\/\/dev-api.popcorn.sg\/)/", base_url(), $r['URL']);
			$r['URL'] = $baseUrl."&ShowType=".$r['ShowType']."&MovieID=".$r['MovieID']."&ShowTime=".$showtime_ori;
			$r['Badge'] = "";
                        $r['RawShowtime'] = $showtime_ori;

			//movie attributes:
			if(sizeof($attributes) > 0){
				$searchKey = array_search($r['MovieID'], array_column($attributes, "MovieID"));
				if($searchKey !== false){ 
					$badgeCinemas =  explode(",", $attributes[$searchKey]["CinemaIDs"]);
					if(in_array($r['CinemaID'], $badgeCinemas)){
						$platform = strtolower($this->platform);
						$badgeArr = json_decode($attributes[$searchKey]["Badge"], true);
						$badge = "";
						if(is_array($badgeArr) && isset($badgeArr[$platform.$postfix])){
							$badge = $badgeArr[$platform.$postfix];
						}

						$r['Badge'] = $badge;
					}
				}
			}

			$keydate = $r["ShowDate"];
			unset($r["ShowDate"]);

			$result[$keydate][$keyMovie][$keyCinema][] = $r;

			$r['MovieNameOriginal'] = $r['MovieName'];

			//check displayname:
			if(isset($r["MovieNameDisplay"]) && $r["MovieNameDisplay"] !== ""){
				$r["MovieName"] = $r["MovieNameDisplay"];
			}
			
			$movies[$keyMovie] = array(
				"MovieName" => $r["MovieName"],
				"MovieNameOriginal" => $r["MovieNameOriginal"],
				"ListingImage" => $r["ListingImage"],
				"Genre" => $r["Genre"],
				"LocalCertification" => $r["LocalCertification"],
				"RunningTime" => $r["RunningTime"],
				"Language" => $r["Language"],
				"HideMovieBlock" => $hidemovieblock
			);

			unset($r["MovieNameDisplay"]);
			unset($r["MovieName"]);
			unset($r["ListingImage"]);
			unset($r["Genre"]);
			unset($r["LocalCertification"]);
			unset($r["RunningTime"]);
			unset($r["Language"]);
		}

		foreach ($refdays as $ref) {
			if(isset($result[$ref])){
				//movie:
				foreach ($result[$ref] as $movieid => $cinemas) {
					//overlay:
					$searchKey = array_search($movieid, array_column($attributes, "MovieID"));
					$posterOverlay = "";
					if($searchKey !== false){
						$platform = strtolower($this->platform);
						$overlayArr = json_decode($attributes[$searchKey]["PosterOverlay"], true);
						if(is_array($overlayArr) && isset($overlayArr[$platform."_3d"])){
							$posterOverlay = $overlayArr[$platform."_3d"];
						}
					}

					$return[$ref][] = array(
						'MovieID' => $movieid, 
						'MovieName' => $movies[$movieid]["MovieName"],
						'MovieNameOriginal' => $movies[$movieid]["MovieNameOriginal"],
						'ListingImage' => $movies[$movieid]["ListingImage"],
						'Genre' => $movies[$movieid]["Genre"],
						'LocalCertification' => $movies[$movieid]["LocalCertification"],
						'RunningTime' => $movies[$movieid]["RunningTime"],
						'Language' => $movies[$movieid]["Language"],
						'HideMovieBlock' => $hidemovieblock,
						'Overlay' => $posterOverlay, 
						'Cinemas' => $cinemas
					);
				}
			}	
		}

		return $return;
	}
	
	private function seat_status($p) {
		$p = (int) $p;
		if ($p == -1) {
			return "0"; //available
		}
		
		if ($p >= 95) {
			return "3";
		} else if ($p >= 60) {
			return "2";
		} else if ($p >= 20) {
			return "1";
		} else if ($p >= 0) {
			return "0";
		}
	}
}
