<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_campaign_m extends MY_Model{
	
	protected $table = 'client_campaigns';
	protected $primary_key = 'ID';
	protected $columns = array(
		'ClientID' => array('Client ID', 'trim|required'),
		'CampaignName' => array('Campaign Name', 'trim|required'),
		'AdTarget' => array('Ad Target', 'trim|required'),
		'AdImage' => array('Ad Image', 'trim|required'),
		'AdUrl' => array('Ad Url', 'trim|required'),
		'mapped_cinemas' => array('Cinemas', 'trim|required'),
		'CinemaIDs' => array('Cinemas', 'trim'),
		'StartDate' => array('Start Date', 'trim'),
		'EndDate' => array('Finish Date', 'trim'),
	);	

	public function __construct()
	{
		parent::__construct();	
		$this->db = $this->load->database('default', TRUE);	
	}		

	public function set_filter($filter)
	{
		$status = element('status', $filter, 2);
		if($status != 2 ){
			$this->db->where('cc.Status', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('cc.CountryCode', $country_code);

		if($campaignname = element('campaignname', $filter)){
			$this->db->like('cc.CampaignName', $campaignname);
		}
	}

	public function get_client_by_term($term, $limit){
		$this->db->select("c.*")
				->from("clients c")
				->like('c.ClientName', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	function insert_client($data){
		if($this->db->insert("clients", $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			return $id;
		}else{
			return false;
		}
	}

	public function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("cc.*, c.ClientName")
				->from("$this->table cc")
				->join("clients c", "c.ID = cc.ClientID")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter,'ID')){
			$this->db->order_by($sort_col, element('sort_dir', $filter, 'DESC'));
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	function insert_campaign($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}


	public function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table cc");
		$row =  $query->row();
		return $row->num;
	}

	public function getClients(){
		$this->db->select("*")
				->from("clients")
				->where("Status", 1);		

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("a.*, c.ClientName")
				->from("$this->table a")
				->join("clients c", "c.ID = a.ClientID")
				->where('a.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_cinemas_by_values($comavalues)
	{
		if($comavalues == "" || !$comavalues){
			return array();
		}

		$this->db->select("CinemaName, ID")
					->from("cinemas")
					->where("ID IN ($comavalues)");
		$query = $this->db->get();
		return $query->result();
	}

	function update_campaign($id, $data){
		$this->db->update($this->table, $data, "ID = ".$id);
		$this->success[] = "Updated successfully";
		return $this->db->affected_rows();
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	function addActivity($data){
		if($this->db->insert("client_campaign_activities", $data))
		{
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			return $id;
		}else{
			return false;
		}
	}

	function addBannerActivity($data){
		if($this->db->insert("client_campaign_banner_activities", $data))
		{
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			return $id;
		}else{
			return false;
		}
	}

	public function getAllActiveCampaigns()
	{
		$this->db->select("cc.*, c.ClientName")
				->from("client_campaigns cc")
				->join("clients c", "c.ID = cc.ClientID")
				->where("cc.Status", 1);	

		$query = $this->db->get();
		return $query->result_array();
	}

	//client campaign report:
	public function getCampaignBannerDaily($ids){
		$now = date("Y-m-d h:i:s");
		$date = date("Y-m-d 00:00:00", strtotime("-30 days", strtotime($now)));
		//$date = date("2016-11-05 00:00:00");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('DATE_FORMAT(triggered_date, "%Y-%m-%d") as DT,count(udid) as Total')
			->where('triggered_date >', $date)
			->where_in('campaign_id', $ids)
			->group_by(array('DT'))
			->order_by('triggered_date', 'asc'); 

		$query = $this->db->get('client_campaign_banner_activities');
		return $query->result_array();
	}	

	public function getCampaignBannerClickDaily($ids){
		$now = date("Y-m-d h:i:s");
		$date = date("Y-m-d 00:00:00", strtotime("-30 days", strtotime($now)));
		//$date = date("2016-11-05 00:00:00");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('DATE_FORMAT(triggered_date, "%Y-%m-%d") as DT,count(udid) as Total')
			->where('triggered_date >', $date)
			->where_in('campaign_id', $ids)
			->group_by(array('DT'))
			->order_by('triggered_date', 'asc');

		$query = $this->db->get('client_campaign_activities');
		return $query->result_array();
	}

	public function getCampaignBannerPerDay($ids, $datestring){
		$dateFrom = date($datestring." 00:00:00");
		$dateTo = date($datestring." 23:59:59");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('cca.campaign_id, cc.CampaignName, count(udid) as Total')
			->where('cca.triggered_date >', $dateFrom)
			->where('cca.triggered_date <', $dateTo)
			->where_in('cca.campaign_id', $ids)
			->join('client_campaigns cc', 'cc.ID = cca.campaign_id')
			->group_by(array('cca.campaign_id', 'cc.CampaignName'))
			->order_by('cca.campaign_id', 'asc'); 

		$query = $this->db->get('client_campaign_banner_activities cca');
		return $query->result_array();
	}

	public function getCampaignBannerClickPerDay($ids, $datestring){
		$dateFrom = date($datestring." 00:00:00");
		$dateTo = date($datestring." 23:59:59");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('cca.campaign_id, cc.CampaignName, count(udid) as Total')
			->where('cca.triggered_date >', $dateFrom)
			->where('cca.triggered_date <', $dateTo)
			->where_in('cca.campaign_id', $ids)
			->join('client_campaigns cc', 'cc.ID = cca.campaign_id')
			->group_by(array('cca.campaign_id', 'cc.CampaignName'))
			->order_by('cca.campaign_id', 'asc'); 

		$query = $this->db->get('client_campaign_activities cca');
		return $query->result_array();
	}

	public function getCampaignBannerDateRange($ids, $datefrom, $dateto){
		$dateFrom = date($datefrom." 00:00:00");
		$dateTo = date($dateto." 23:59:59");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('cca.campaign_id, cc.CampaignName, count(udid) as Total')
			->where('cca.triggered_date >', $dateFrom)
			->where('cca.triggered_date <', $dateTo)
			->where_in('cca.campaign_id', $ids)
			->join('client_campaigns cc', 'cc.ID = cca.campaign_id')
			->group_by(array('cca.campaign_id', 'cc.CampaignName'))
			->order_by('cca.campaign_id', 'asc'); 

		$query = $this->db->get('client_campaign_banner_activities cca');
		return $query->result_array();
	}

	public function getCampaignBannerClickDateRange($ids, $datefrom, $dateto){
		$dateFrom = date($datefrom." 00:00:00");
		$dateTo = date($dateto." 23:59:59");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('cca.campaign_id, cc.CampaignName, count(udid) as Total')
			->where('cca.triggered_date >', $dateFrom)
			->where('cca.triggered_date <', $dateTo)
			->where_in('cca.campaign_id', $ids)
			->join('client_campaigns cc', 'cc.ID = cca.campaign_id')
			->group_by(array('cca.campaign_id', 'cc.CampaignName'))
			->order_by('cca.campaign_id', 'asc'); 

		$query = $this->db->get('client_campaign_activities cca');
                
		return $query->result_array();
	}

	public function getCampaignBannerVisitDateRange($ids, $datefrom, $dateto){
		$dateFrom = date($datefrom." 00:00:00");
		$dateTo = date($dateto." 23:59:59");

		$this->db->_protect_identifiers = FALSE;
		$this->db->select('cca.campaign_id, cc.CampaignName, count(DISTINCT cca.udid) as Total', false)
			->where('cca.triggered_date >', $dateFrom)
			->where('cca.triggered_date <', $dateTo)
			->where_in('cca.campaign_id', $ids)
			->where('( 3959 * acos( cos( radians(1.3008843) ) * cos( radians( ba.lat ) ) * cos( radians( ba.lng ) - radians(103.8371984)) + sin( radians(1.3008843) ) * sin( radians( ba.lat ) ) ) ) < 1.5
',null)
			->join('client_campaigns cc', 'cc.ID = cca.campaign_id')
            ->join('beacons_analytics ba', 'ba.UDID = cca.UDID and ba.triggered_date >= cca.triggered_date and ba.uuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"', '', false)
			->group_by(array('cca.campaign_id', 'cc.CampaignName'))
			->order_by('cca.campaign_id', 'asc'); 

		$query = $this->db->get('client_campaign_banner_activities cca');

		return $query->result_array();
	}

}
