<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mastermovie_m extends MY_Model{
	
	protected $table = 'master_movies';
	protected $primary_key = 'ID';
	protected $columns = array(
		'MasterName' => array('MasterName', 'trim|required'),
		'Cast' => array('Cast', 'trim|required'),
		'Director' => array('Director', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}	
	
	public function set_filter($filter)
	{
		if($name = element('name', $filter)){
			$this->db->like('c.MasterName', $name);
		}
	}

	public function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("c.*")
				->from("$this->table c")
				->limit($limit, $offset);		
		
		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_term($term, $limit){
		$this->db->select("c.*")
				->from("$this->table c")
				->like('c.Mastername', $term)
				->limit($limit, 0);

		$query = $this->db->get();
		return $query->result();
	}

	public function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("$this->table c");
		$row =  $query->row();
		return $row->num;
	}

	public function getById($movieid){
		$this->db->select("m.*")
				->from("$this->table m")
				->where('m.ID', $movieid);

		$query = $this->db->get();
		return $query;
	}

	public function getAllChild($movieid){
		$this->db->select("m.*")
				->from("movies m")
				->where('m.MasterID', $movieid);

		$query = $this->db->get();
		return $query->result();
	}

	function insert_master_movie($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			$id = $this->db->insert_id();
			if(!$id) $id = true;
			$this->_after_insert($id);
			return $id;
		}else{
			return false;
		}
	}

	function update_movie($id, $data){
		$this->db->update("movies", $data, "ID = ".$id);
		return $this->db->affected_rows();
	}

}
