<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_accounts_m extends MY_Model{
	
	protected $table = 'user_accounts';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByEmail($email, $usertype){
		$this->db->select("*")
				->from($this->table)
				->where('email', $email)
				->where('user_type', $usertype);

		$query = $this->db->get();
		return $query;
	}

	public function getByRefCode($refcode){
		$this->db->select("*")
				->from($this->table)
				->where('ref_code', $refcode);

		$query = $this->db->get();
		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return null;
		}
	}

	public function insertData($data){
		if($this->db->insert($this->table, $data)){
			$id = $this->db->insert_id();
			if(!$id) $id = false;
			return $id;
		}else{
			return false;
		}
	}

	function update_user($id, $data){
		$this->db->update($this->table, $data, "id = ".$id);
		return $this->db->affected_rows();
	}

	public function getTotalGenders($ids){
		$this->db->select("gender, count(gender) as Total")
				->from($this->table)
				->where_in('id', $ids)
				->where('gender IS NOT NULL', null)
				->group_by(array('gender'));

		$query = $this->db->get();
		return $query;
	}

	public function getUsersAges($ids){
		$this->db->select("TIMESTAMPDIFF(YEAR, birthday, NOW()) as Age")
				->from($this->table)
				->where_in('id', $ids)
				->where("birthday not in ('0000-00-00', '1970-01-01')", null);

		$query = $this->db->get();
		return $query;
	}

	public function getById($userid, $usertype = ""){
		$this->db->select("*")
				->from($this->table)
				->where('id', $userid);
				
		if($usertype != ""){
			$this->db->where('user_type', $usertype);
		}

		$query = $this->db->get();
		return $query->row_array();
	}

	public function getExistingFav($type, $fav_id, $userid){
		$this->db->select("*")
				->from("user_accounts_favs")
				->where('fav_type', $type)
				->where('userid', $userid)
				->where('fav_id', $fav_id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return null;
	    }
	}

	public function getUsersFav($userid){
		$this->db->select("*")
				->from("user_accounts_favs")
				->where('userid', $userid);

		$query = $this->db->get();
		return $query->result();
	}

	public function insert_fav($data){
		if($this->db->insert("user_accounts_favs", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function del_fav($type, $fav_id, $userid){
		$this->db->where('userid', $userid);
		$this->db->where('fav_type', $type);
		$this->db->where('fav_id', $fav_id);
		return $this->db->delete("user_accounts_favs");
	}

	public function getCompleteBio($userid){
		$this->db->select("ua.name, ua.email, ua.aboutme, ua.genres, SUM(if(uaf.fav_type = 'movie', 1, 0)) AS movie_total, SUM(if(uaf.fav_type = 'tv', 1, 0)) AS tv_total, SUM(if(uaf.fav_type = 'person', 1, 0)) AS person_total", false)
			->from("user_accounts ua")
			->join("user_accounts_favs uaf", "ua.id = uaf.userid")
			->where('ua.id', $userid);
		
		$query = $this->db->get();
		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return null;
		}
	}

	public function generate_referral_code() {
        $code = substr(substr(md5(rand()), 0, 8), -6);
        $this->db->select("*")
				->from("user_accounts")
				->where('ref_code', $code);
		$query = $this->db->get();

        // Loop till this code is unique.
        while ($query->num_rows() == 1) {
            $code = substr(substr(md5(rand()), 0, 8), -6);
            $this->db->select("*")
				->from("user_accounts")
				->where('ref_code', $code);
			$query = $this->db->get();
        }

        return $code;
    }
}
