<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_main_m extends MY_Model{

	protected $table = 'app_banner_main';
	protected $primary_key = 'id';
	protected $columns = array(
		'country_code' => array('Country', 'trim|required'),
		'url' => array('Source URL', 'trim|required'),
		'banner_thumb' => array('Banner Thumb', 'trim'),
		'location' => array('Location', 'trim|required'),
		'type' => array('Type', 'trim|required'),
		'action_url' => array('Action URL', 'trim', NULL, ""),
		'action_caramel_url' => array('Action Caramel URL', 'trim', NULL, ""),
		'code_tracking' => array('Code Tracking', 'trim', NULL, ""),
		'IsAB' => array('Is A/B Testing', 'trim', NULL, 0),
		'start_date' => array('Start Date', 'trim|required'),
		'end_date' => array('End date', 'trim|required'),
		'last_image' => array('Last Image', 'trim', NULL, "")		
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	function getBannerLocation(){
		$type = array();
		$type[0]["value"] = "search";
		$type[0]["text"] = "Search";
		return $type;
	}

	function getBannerType(){
		$type = array();
		$type[0]["value"] = "video";
		$type[0]["text"] = "Video";
		$type[1]["value"] = "image";
		$type[1]["text"] = "Image";
		$type[2]["value"] = "website";
		$type[2]["text"] = "Website";
		return $type;
	}

	function set_filter($filter)
	{ 
		$location = element('location', $filter, 1);
		$this->db->where('b.location', $location);
		$status = element('status', $filter, 3);
		if($status != 3 ){
			$this->db->where('b.is_active', $status);
		}

		$country_code = element('country_code', $filter, 'SG');
		$this->db->where('b.country_code', $country_code);
	}

	function get_items($filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("b.*")
				->from("app_banner_main b")
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('last_updated', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($id){
		$this->db->select("b.*")
				->from("app_banner_main b")
				->where('b.id', $id);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_items_by_location($location, $country){
		$this->db->select("b.*")
				->from("app_banner_main b")
				->where('b.is_active', 1)
				->where('b.country_code', $country)
				->where('b.location', $location);

		$query = $this->db->get();
		return $query->result();
	}

	function get_count($filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$query = $this->db->get("app_banner_main b");
		$row = $query->row();
		return $row->num;
	}

	function getAllABBanner(){
		$this->db->select("*")
				->from("app_banner_main")
				->where('IsAB', 1);

		$query = $this->db->get();
		return $query->result_array();
	}

	function get_recent_gallery(){
		$this->db->select("url")
				->from("app_banner_main")
				->where('type', 'image')
				->order_by('last_updated', 'DESC')
				->limit(15, 0);

		$query = $this->db->get();
		return $query->result();
	}

}