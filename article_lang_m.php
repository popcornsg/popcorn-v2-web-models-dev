<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_lang_m extends MY_Model{

	/*
	*	Status:
	*   1 = Draft
	* 	2 = Publish
	* 	3 = Pending
	*/

	protected $table = 'article_langs';
	protected $primary_key = 'id';
	protected $columns = array(
		'title' => array('Title', 'trim|required'),
		'content' => array('Content Language', 'trim|required'),
		'lang_code' => array('Language', 'trim|required'),
		'article_id' => array('Article ID', 'trim|required')
	);

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}

	function set_filter($filter)
	{ 
		$lang = element('lang_code', $filter, "");
		if($lang != ""){
			$this->db->where('lang_code', $lang);
		}
	}

	function get_items($articleid, $filter, $offset, $limit)
	{
		$this->set_filter($filter);
		$this->db->select("*")
				->from("$this->table")
				->where("article_id", $articleid)
				->limit($limit, $offset);

		if($sort_col = element('sort_col', $filter)){
			$this->db->order_by($sort_col, element('sort_dir', $filter));
		}else{
			$this->db->order_by('title', 'DESC');
		}

		$query = $this->db->get();
		return $query->result();
	}

	function get_pagination($offset, $limit, $country_code)
	{
		$this->db->select("*")
				->from("$this->table")
				->where('status', 2)
				->where('country_code', $country_code)
				->order_by('published_date', 'DESC')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result();
	}

	function get_record_complete($articleid, $langcode){
		$this->db->select("a.*")
				->from("$this->table a")
				->where('a.article_id', $articleid)
				->where('a.lang_code', $langcode);

		$query = $this->db->get();
		if ($query->num_rows() == 1) {
	        return $query->row();
	    }else{
	    	return $this->get_empty_record();
	    }
	}

	function get_all_languages($articleid){
		$this->db->select("a.lang_code")
				->from("$this->table a")
				->where('a.article_id', $articleid);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function getById($articleid){
		$this->db->select("*")
				->from("articles a")
				->where('a.id', $articleid);

		$query = $this->db->get();
		return $query;
	}

	public function getExisting($articleid, $langcode){
		$this->db->select("*")
				->from("article_langs a")
				->where('a.article_id', $articleid)
				->where('a.lang_code', $langcode);;

		$query = $this->db->get();
		return $query;
	}

	public function update_lang($articleid, $langcode, $data){
		$this->db->update('article_langs', $data, array('article_id'=>$articleid, 'lang_code' => $langcode));
	}

	function insert_lang($data){
		if($this->db->insert($this->table, $data))
		{
			$this->success[] = "Added successfully";
			return true;
		}else{
			return false;
		}
	}

	function delete_article_lang($articleid, $langcode){
		return $this->db->delete('article_langs', array('article_id' => $articleid, 'lang_code' => $langcode)); 
	}

	function check_validation($cond = 0, $cols = NULL){
		$fields = $this->get_from_post($cols, $cond);
		if($fields == false){
			return false;
		}

		return true;
	}

	function get_count($articleid, $filter)
	{
		$this->set_filter($filter);
		$this->db->select('count(*) as num');
		$this->db->where('article_id', $articleid);
		$query = $this->db->get("$this->table");
		$row =  $query->row();
		return $row->num;
	}

}