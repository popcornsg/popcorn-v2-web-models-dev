<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_point_history_m extends MY_Model{
	
	protected $table = 'user_point_histories';
	protected $primary_key = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByUserID($userid){
		$this->db->select("*")
				->from($this->table)
				->where('userid', $userid);

		$query = $this->db->get();
		return $query;
	}

	public function getByAction($userid, $action, $movieid = null){
		$this->db->select("*")
				->from($this->table)
				->where('action', $action)
				->where('userid', $userid);

		if(isset($movieid)){
			$this->db->where('movie_id', $movieid);
		}

		$query = $this->db->get();
		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return null;
		}
	}

	function getNotificationEarnedPoints($userid){
		$this->db->select("SUM(earned_points) as earned_point", false)
				->from($this->table)
				->where('notified', 0)
				->where('userid', $userid);

		$query = $this->db->get();
		if($query->num_rows() == 1){
			$point = $query->row()->earned_point;

			//updated all notif:
			$this->db->where('userid', $userid);
			$this->db->set('notified', 1, FALSE);
			$this->db->update($this->table);

			return $point;
		}else{
			return 0;
		}
	}

	public function getUserPointHistory($userid, $offset, $limit){
		$this->db->select("*")
				->from("$this->table ")
				->where('userid', $userid)
				->order_by('created_date', 'DESC')
				->limit($limit, $offset);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function isValidAction($userid, $action, $rule){
		if($rule["rule"] == NORULE){
			return true;
		}else{
			$this->db->select("*")
				->from($this->table)
				->where('action', $action)
				->where('userid', $userid);

			//checking one time conditions:
			if($rule["rule"] == RULE_ONCE){
				$query = $this->db->get();
				if($query->num_rows() == 0){
					return true;
				}else{
					return false;
				}
			}else if($rule["rule"] == RULE_DAILY){
				$startdate = date('Y-m-d 00:00:00');
				$enddate = date('Y-m-d 23:59:59');
				$this->db->where('created_date >=', $startdate)
					->where('created_date <=', $enddate);
				$query = $this->db->get();
				if($query->num_rows() < $rule["threshold"]){
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}
	}

	function insertData($data)
	{ 
		return $this->db->insert($this->table, $data);
	}

	function updateUserPointInfo($userid, $data){
		$this->db->update("user_accounts", $data, "id = ".$userid);
	}

	function getCurrentUserPointInfo($userid){
		$this->db->select("*")
				->where('id', $userid)
				->from("user_accounts");

		$query = $this->db->get();
		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return null;
		}
	}

}
